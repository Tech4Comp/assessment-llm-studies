import os
import streamlit as st
import json
import sys
from langchain.prompts import PromptTemplate
from langchain.callbacks import get_openai_callback
from pydantic import BaseModel, Field
from typing import List
from jsondiff import diff
import re
import glob
from utils import createConversation, JSONOutputParser, load_model, writeItemToSite, formatItemForPrompt, getInstalledModels

st.set_page_config(layout="wide")

# ########################### PREPARE PROMPTs ###########################

ak_context = open("./itemToTaxonomy_prompts/ak_context.txt").read()

# from https://learnlab.org/wiki/images/9/9f/Krathwohl_Bloom%27s_taxonomy_revised.pdf
taxonomy_description = open("./itemToTaxonomy_prompts/taxonomy_description.txt").read()

output_formatter = """
### Output Format ###
Answer with json only, according to the following template, using lower case words:
[{{
    "knowledgeDimension": "",
    "processDimension": ""
}}]
"""

task = """
### Instruction ###
For various item-types, learners are required to determine the meaning of the given instructions (Aufgabe), interpret the given options (Antwortoptionen) and choose the correct ones. Thus, learners need to understand the task, anlyse the given options, and evaluate their content with respect to the task. Avoid considering the processes of learners understanding, analysing and evaluating the assessment item itself.

Classify the following assessment item written in German according to the given taxonomy matrix. If there are multiple classifications that apply, provide all of them in your response.

Assessment Item:
```
{item}
```
"""

classify_ak_prompt = PromptTemplate(
    template=ak_context + taxonomy_description + task + output_formatter,
    input_variables=["item"]
)

prompt_classification_justification = """
Please justify your answer for each pair of knowledge dimension and process dimension briefly. For the considered process dimension, point out which process is used by learners. For the knowledge dimension, point out which specific knowledge is used.
"""

json_parser = JSONOutputParser()

# ########################### CREATE STREAMLIT-APP ###########################

st.title('Item taxonomy classification using LLMs')
st.warning("This is a research prototype and might produce misleading results. Check everything thoroughly!")

models = getInstalledModels()
chosenModel = st.selectbox('Model', (models), format_func=lambda path: re.split('/', path)[-1])
if chosenModel != '':
    llm = load_model(chosenModel)

conversation, memory = createConversation(llm)

if 'costs' not in st.session_state:
    st.session_state['costs'] = 0

uploaded_file = st.file_uploader('Items', type=['json','jsonld'], help='A file that contains one or more items')

items: list = None
if uploaded_file is not None:
    items = json.load(uploaded_file)

processNumberOfItems = st.number_input("Stop after x Items:", min_value=1, value=1, step=1)


st.write("Taxonomy: Anderson & Krathwohl (24 levels)")

if items is not None and len(items) > 0:
    with st.spinner('Processing...'):
        with get_openai_callback() as cb:
            for index, item in enumerate(items['@graph']):
                writeItemToSite(item)
                text = formatItemForPrompt(item)
                output = conversation.predict(input=classify_ak_prompt.format(item=text))
                print(output)
                if item.get('annotations') is None:
                    item['annotations'] = {}
                try:
                    extracted_json = json.loads("" + json_parser.parse(output))
                    item['annotations']['akLLM'] = extracted_json
                    st.json(extracted_json)
                    with st.expander("See explanation"):
                        output = conversation.predict(input=prompt_classification_justification)
                        item['annotations']['akLLMExplanation'] = output
                        st.write(output)
                    st.divider()
                except ValueError as error:
                    st.write("Classification not possible: " + error)
                finally:
                    memory.clear()

                if index + 1 == processNumberOfItems:
                    break
        print(cb)
        st.session_state.costs = st.session_state.costs + cb.total_cost
    st.success('Done!')
    with open('result.json', 'w', encoding='utf-8') as f:
        json.dump(items, f, ensure_ascii=False, indent=2)

if st.session_state.costs:
    st.write("Total Cost (USD Cent): ¢{:.4f}".format((st.session_state.costs * 100)))