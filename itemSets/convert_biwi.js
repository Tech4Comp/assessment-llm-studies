#! /usr/bin/env node
const fs = require('fs');
const data = require('./biwi_csv.json');

for (item of data) {
  item["@id"] = item.id
  delete item.id
  item.task = item.question
  delete item.question
  if(item.type === "SC" && item["Column7"] !== undefined) {
    item["@type"] = "http://tech4comp/eal/SingleChoiceItem"
    delete item.type
    item.answers = []
    for (let i = 0; i < 8; i= i + 2) {
      let toInsert = { "@type": "http://tech4comp/eal/Answer" }
      toInsert.text = "" + item['Column' + (7 + i)]
      toInsert.points = item['Column' + (8 + i)]
      item.answers.push(toInsert)
    }
    for (let i = 7; i <= 23; i++) {
      delete item['Column' + i]
    }
    delete item[""]
    // console.log(item)
  }

  if(item["type"] === "MC" && item["Column7"] !== undefined) {
    item["@type"] = "http://tech4comp/eal/MultipleChoiceItem"
    delete item.type
    item.answers = []
    for (let i = 0; i <= 12; i = i + 3 ) {
      let toInsert = { "@type": "http://tech4comp/eal/Answer" }
      toInsert.text = "" + item['Column' + (9 + i)]
      toInsert.points = item['Column' + (8 + i)]
      toInsert.pointsNotSelected = item['Column' + (7 + i)]
      item.answers.push(toInsert)
    }
    for (let i = 7; i <= 23; i++) {
      delete item['Column' + i]
    }
    delete item[""]
    // console.log(item)
  }

  item.levelAnnotation = {}
  item.levelAnnotation.classifier1 = item.NP
  item.levelAnnotation.classifier2 = item.FH
  item.levelAnnotation.classifier3 = item.FF
  item.levelAnnotation.chosen = item.level
  if(item.levelAnnotation.chosen === "")
  item.levelAnnotation.chosen = Math.round((item.levelAnnotation.classifier1 + item.levelAnnotation.classifier2 + item.levelAnnotation.classifier3) / 3)
  delete item.level
  delete item.NP 
  delete item.FH 
  delete item.FF
}

let result = {
  "@context": {
    "modificationTime": "http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#dateModified",
    "creationTime": "http://purl.org/healthcarevocab/v1#CreationTime",
    "title": "http://purl.org/dc/terms/title",
    "description": "http://tech4comp/eal/context",
    "task": "http://tech4comp/eal/task",
    "images": {
      "@id": "http://schema.org/image",
      "@container": "@set"
    },
    "difficulty": {
      "@id": "http://tech4comp/eal/difficulty",
      "@type": "http://www.w3.org/2001/XMLSchema#integer"
    },
    "expectedSolvability": {
      "@id": "http://tech4comp/eal/expectedSolvability",
      "@type": "http://www.w3.org/2001/XMLSchema#double"
    },
    "performanceLevel": {
      "@id": "http://tech4comp/eal/hasPerformanceLevel",
      "@type": "@id"
    },
    "knowledgeDimension": {
      "@id": "http://tech4comp/eal/hasKnowledgeDimension",
      "@type": "@id"
    },
    "learningOutcome": {
      "@id": "http://tech4comp/eal/hasLearningOutcome",
      "@type": "@id"
    },
    "annotations": {
      "@id": "http://tech4comp/eal/annotation"
    },
    "flagged": {
      "@id": "http://tech4comp/eal/flagged",
      "@type": "http://www.w3.org/2001/XMLSchema#boolean"
    },
    "note": "http://www.w3.org/2004/02/skos/core#note",
    "bloom": {
      "@id": "http://tech4comp/eal/hasBloomMapping",
      "@container": "@set"
    },
    "points": {
      "@id": "http://tech4comp/eal/points",
      "@type": "http://www.w3.org/2001/XMLSchema#decimal"
    },
    "status": "http://tech4comp/eal/status",
    "project": {
      "@id": "http://tech4comp/eal/hasProject",
      "@type": "@id"
    },
    "contributors": {
      "@id": "http://purl.org/dc/terms/contributor",
      "@container": "@set",
      "@type": "@id"
    },
    "topics": {
      "@id": "http://purl.org/dc/terms/subject",
      "@container": "@set",
      "@type": "@id"
    },
    "answers": {
      "@id": "http://tech4comp/eal/hasAnswer",
      "@container": "@list"
    },
    "text": {
      "@id": "http://tech4comp/eal/answerText"
    },
    "pointsNotSelected": {
      "@id": "http://tech4comp/eal/pointsNotSelected",
      "@type": "http://www.w3.org/2001/XMLSchema#decimal"
    },
    "maximumSelectableAnswers": {
      "@id": "http://tech4comp/eal/maximumSelectableAnswers",
      "@type": "http://www.w3.org/2001/XMLSchema#integer"
    },
    "minimumSelectableAnswers": {
      "@id": "http://tech4comp/eal/minimumSelectableAnswers",
      "@type": "http://www.w3.org/2001/XMLSchema#integer"
    },
    "remoteItemURL": "http://tech4comp/eal/remoteItemURL",
    "sequenceStep": {
      "@id": "http://tech4comp/eal/sequenceStep",
      "@type": "http://www.w3.org/2001/XMLSchema#integer"
    }
  },
  "@graph": data
}

fs.writeFileSync('biwi.json', JSON.stringify(result));