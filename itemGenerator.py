import json
import uuid

from langchain.embeddings import HuggingFaceEmbeddings
from langchain.output_parsers import PydanticOutputParser, OutputFixingParser

from task_checker import *
from langchain.callbacks import get_openai_callback
from langchain.chains import RetrievalQA
from langchain.document_loaders import PyPDFLoader, TextLoader, DirectoryLoader
from owlready2 import *

from apikey import apikey
from langchain import FAISS
from langchain.chat_models import ChatOpenAI
from langchain import LLMChain
import streamlit as st
from pydantic import BaseModel, Field

# ########################### LLM CONFIGURATION ###########################

os.environ['OPENAI_API_KEY'] = apikey

# For information retrieval
llm = ChatOpenAI(temperature=0, openai_api_key=apikey, model_name="gpt-3.5-turbo")
# For information retrieval
llm_16k = ChatOpenAI(temperature=0, openai_api_key=apikey, model_name="gpt-3.5-turbo-16k")
# For agent
llm_gpt4 = ChatOpenAI(temperature=0, openai_api_key=apikey, model_name="gpt-4")

# ########################### READ OWL ###########################

onto_path.append("sources/INB_MIB_Mathematik_1.owl")
onto = get_ontology("sources/INB_MIB_Mathematik_1.owl")
onto.load()
process_dimensions = list(onto.Prozessdimension.subclasses())
knowledge_dimensions = list(onto.Wissensdimension.subclasses())
concepts = list(onto.Mathematisches_Konzept.instances())

# ########################### STREAMLIT-CONFIG ###########################

st.set_page_config(layout="wide")
st.title('🔢 LLM-MATH-TASK-GENERATOR')
st.caption('Modell: OpenAI GPT-4')

if 'task' not in st.session_state:
    st.session_state['task'] = None
if 'parsed_task' not in st.session_state:
    st.session_state['parsed_task'] = None
if 'knowledge' not in st.session_state:
    st.session_state['knowledge'] = None
if 'lecture' not in st.session_state:
    st.session_state['lecture'] = None
if 'pd_info' not in st.session_state:
    st.session_state['pd_info'] = None
if 'kd_info' not in st.session_state:
    st.session_state['kd_info'] = None

score_threshold = 0.5
with st.sidebar:
    st.title('Einstellungen')
    selected_concept = st.selectbox('Welches mathematische Konzept soll erzeugt werden?',
                                    sorted([c.name for c in concepts], key=str.casefold))
    llm_type = st.selectbox('Welches Modell soll verwendet werden (Suche)?', ['gpt-3.5-turbo-16k', 'gpt-4'])
    chain_type = st.selectbox('Welche Chain soll verwendet werden?', ['stuff', 'map_reduce', 'map_rerank', 'refine'])
    search_algo = st.selectbox('Welcher Algorithmus soll verwendet werden?', ['mmr', 'similarity_score_threshold'])
    k = st.slider('Anzahl Dokumente ', min_value=1, max_value=30, value=10)
    if search_algo == "similarity_score_threshold":
        score_threshold = st.slider('Similarity Threshold', min_value=.1, max_value=1.0, value=.8)

# ########################### DOCUMENT RETRIEVER ###########################
try:
    lecture_retriever = FAISS.load_local("lecture_index", HuggingFaceEmbeddings())
except:
    lecture_doc = PyPDFLoader("sources/HTWK_Mathematik_I_INB_MIB.pdf").load_and_split()
    lecture_retriever = FAISS.from_documents(lecture_doc, HuggingFaceEmbeddings())
    lecture_retriever.save_local("lecture_index")

lecture_chain = RetrievalQA.from_chain_type(llm=llm, chain_type="map_reduce",
                                            retriever=lecture_retriever.as_retriever())

try:
    knowledge_retriever = FAISS.load_local("knowledge_index2", HuggingFaceEmbeddings(model_name="thenlper/gte-large"))
except:
    knowledge_doc = DirectoryLoader("sources/math_knowledge/",
                                    glob="**/**/*.pdf",
                                    use_multithreading=True,
                                    loader_cls=PyPDFLoader,
                                    show_progress=True).load_and_split()
    knowledge_retriever = FAISS.from_documents(knowledge_doc, HuggingFaceEmbeddings(model_name="thenlper/gte-large"))
    knowledge_retriever.save_local("knowledge_index2")

knowledge_chain = RetrievalQA.from_chain_type(llm=llm_gpt4 if llm_type == "gpt-4" else llm_16k,
                                              chain_type=chain_type,
                                              retriever=knowledge_retriever.as_retriever(search_type=search_algo,
                                                                                         search_kwargs={
                                                                                             "score_threshold": score_threshold,
                                                                                             "k": k,
                                                                                         }),
                                              return_source_documents=True)


# ########################### OUTPUT PARSER ###########################

class Task(BaseModel):
    task: str = Field(description="Aufgabenstellung")
    solution: str = Field(description="Lösung")

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4, ensure_ascii=False)


task_parser = PydanticOutputParser(pydantic_object=Task)
auto_fix_parser = OutputFixingParser.from_llm(parser=task_parser, llm=llm_gpt4)

# ########################### TASK GENERATOR ###########################

prompt = PromptTemplate(
    input_variables=["concept_information",
                     "pd_information",
                     "kd_information",
                     "kd",
                     "pd",
                     "concept",
                     "lecture_process"],
    template="""
            Create a math exercise on the mathematical concept {concept} representing the process dimension {pd}
            and the knowledge dimension {kd}.
    
            ### Instructions ###
            
            1. The tasks should contain mathematical annotations and formulas in latex format!
            2. The tasks should be formulated in German!
            3. The tasks should contain random numbers and not just enumerated numbers (1,2,3,4,5,...)!
            4. The task should not provide any hints, help, examples or explanations!
            5. Think step by step about how to generate this task correctly
               
            ### Context ###
            
            These generated tasks are intended for first-semester Bachelor's students in computer science, who are to 
            be individually supported and challenged so that they satisfy the demands of the mathematics module.            
            The students are presented with the generated tasks by an automatic recommendation system and can respond
            to them in text form. The tasks are selected personalised for the students according to their level of 
            competence (Represented by a knowledge dimension and process dimension) and current relevant concepts. 
            Therefore, it is extremely important that the tasks only fit the specified mathematical concept, 
            the process dimension and the knowledge dimension!
            
            {lecture_process}
            
            ### KNOWLEDGE ###
            
            Use the following knowledge (concept information) to create te task: 
                {concept_information}
            
            Tasks on the process dimension {pd} follow these rules:
                {pd_information}
            
            Tasks on the knowledge dimension {kd} follow these rules: 
                {kd_information}
                                                     
            {format_instructions}
    """,
    partial_variables={"format_instructions": task_parser.get_format_instructions()}
)
creation_chain = LLMChain(llm=llm_gpt4, prompt=prompt)

# ########################### WRITE OWL ###########################

@st.cache_data
def save_in_domain_model(_task, pd, kd, c):
    print("Saving model...")
    onto_concept = [con for con in concepts if con.name == c][0]
    onto_pd = [pro_dim for pro_dim in process_dimensions if pro_dim.name == pd][0]
    onto_kd = [kno_dim for kno_dim in knowledge_dimensions if kno_dim.name == kd][0]

    test = onto.Aufgabe("generated_task_" + onto_concept.name + "_" +
                        onto_pd.name + "_" +
                        onto_kd.name)
    with onto:
        if len(test.Aufgabenbeschreibung) == 0:
            test.Aufgabenbeschreibung.append(_task.task)
            test.Lösung.append(_task.solution)
        else:
            test.Aufgabenbeschreibung = _task.task
            test.Lösung = _task.solution
        test.adressiert_PD.append(onto_pd)
        test.adressiert_WD.append(onto_kd)
        test.uebt.append(onto_concept)
        onto_concept.wird_geuebt_in.append(test)

    onto.save(file="sources/INB_MIB_Mathematik_1.owl", format="rdfxml")


# ########################### WRITE TRAIN-SET ###########################

@st.cache_data
def save_in_train_set(_completion, pd, kd, concept,
                      concept_information, _source_docs, process_dimension_information,
                      knowledge_dimension_information):
    file_path = "train_set.json"

    new_element = {
        "id": str(uuid.uuid4()),
        "math_concept": concept,
        "knowledge_dimension": kd,
        "process_dimension": pd,
        "knowledge_dimension_information": knowledge_dimension_information,
        "process_dimension_information": process_dimension_information,
        "concept_information": concept_information,
        "source_documents": [{
            "page_content": p.page_content,
            "metadata": p.metadata} for p in _source_docs],
        "prompt": prompt.template,
        "completion": {
            "task": _completion.task,
            "solution": _completion.solution
        }
    }

    with open(file_path, "r", encoding="utf8") as json_file:
        data = json.load(json_file)

    data.append(new_element)

    with open(file_path, "w", encoding="utf8") as json_file:
        json.dump(data, json_file, ensure_ascii=False)


# ########################### STREAMLIT-APP ###########################

if st.button('Konzeptinformationen generieren'):
    with st.spinner('Processing...'):
        start = time.time()
        st.session_state['knowledge'] = knowledge_chain(
            {"query": f"""According to the provided documents,
                Explain the mathematical concept "{selected_concept}" in a comprehensive section. 

                Only give an example, if one is presented in the document and refer to the documents. 

                Let's think step by step about how to solve this question, here are the provided documents that
                might help you. Say i don't know if you are unable to answer based on the documents provided

                Answer solely based on the documents provided.

                Describe the question in detail before attempting to answer.

                Answer in german.
                """})
        end = time.time()
        st.info(f"Total Time: ${end - start}s")

def parse_task(raw_task):
    try:
        parsed_task = task_parser.parse(raw_task)
    except:
        print("fixing parser ...")
        parsed_task = auto_fix_parser.parse(raw_task)
    return parsed_task

def create_task(process_dimension, knowledge_dimension):
    with open(f'sources/process_dimensions/{process_dimension}.txt', 'r',
              encoding="utf-8") as pd_file, open(
        f'sources/knowledge_dimensions/{knowledge_dimension}.txt', 'r',
        encoding="utf-8") as kd_file:
        st.session_state.pd_info = pd_file.read()
        st.session_state.kd_info = kd_file.read()

    st.session_state.task = creation_chain.run(
        concept_information=st.session_state['knowledge']["result"],
        pd_information=st.session_state.pd_info,
        kd_information=st.session_state.kd_info,
        lecture_process=st.session_state['lecture'],
        pd=process_dimension,
        kd=knowledge_dimension,
        concept=selected_concept)

    st.session_state['parsed_task'] = parse_task(st.session_state.task)

    save_in_domain_model(st.session_state.parsed_task,
                         process_dimension,
                         knowledge_dimension,
                         selected_concept)
    save_in_train_set(st.session_state.parsed_task,
                      process_dimension,
                      knowledge_dimension,
                      selected_concept,
                      st.session_state['knowledge']["result"],
                      st.session_state['knowledge']["source_documents"],
                      st.session_state.pd_info,
                      st.session_state.kd_info)

if st.session_state['knowledge'] is not None:
    st.session_state['knowledge']['result'] = st.text_area("Wissen", st.session_state['knowledge']['result'])
    with st.expander("Verwendete Dokumente"):
        st.write(st.session_state['knowledge']['source_documents'])

    use_student_knowlede = st.checkbox("Wissensstand Studenten einbeziehen?", value=False)
    if use_student_knowlede:
        if st.button('Wissensstand Studenten schätzen'):
            with st.spinner('Processing...'):
                st.session_state['lecture'] = lecture_chain.run(
                    {"query": f"""According to the provided lecture script,
                     
                     List in bullet points what a leaner should know about the {selected_concept} 
                     after it has been covered as in the lecture script.
                     
                     Answer in german.
                     """}
                )
        st.session_state['lecture'] = st.text_area("Kenntnisstand", st.session_state['lecture'])
    else:
        st.session_state['lecture'] = ""

    create_all = st.checkbox("Alle Kompetenzstufen erzeugen", value=False)
    if not create_all:
        process_dimension = st.selectbox('Welche Prozessdimension soll erzeugt werden?',
                                         [pd.name for pd in process_dimensions])
        knowledge_dimension = st.selectbox('Welche Wissensdimension soll erzeugt werden?',
                                           [kd.name for kd in knowledge_dimensions])

    if st.button('Aufgabe(n) erzeugen'):
        with st.spinner('Processing...'):
            if create_all:
                for pd in process_dimensions:
                    for kd in knowledge_dimensions:
                        create_task(pd.name,  kd.name)
            else:
                create_task(process_dimension, knowledge_dimension)

if st.session_state.parsed_task:
    st.caption('Aufgabe')
    st.write(st.session_state.parsed_task.task)
    st.caption('Lösung')
    st.write(st.session_state.parsed_task.solution)
    if st.button("Check"):
        with st.spinner('Processing...'):
            checked_and_fixed_task = check_and_fix_task(st.session_state.parsed_task)
            caft = parse_task(checked_and_fixed_task)
            with st.expander("caft"):
                st.caption('Aufgabe')
                st.write(caft.task)
                st.caption('Lösung')
                st.write(caft.solution)
    if st.button("Schwerer"):
        with st.spinner('Processing...'):
            md_task = more_difficult(st.session_state.parsed_task)
            mdpt = parse_task(md_task)
            with st.expander("mdpt"):
                st.caption('Aufgabe')
                st.write(mdpt.task)
                st.caption('Lösung')
                st.write(mdpt.solution)