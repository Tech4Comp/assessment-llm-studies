import os
from langchain.experimental.plan_and_execute import PlanAndExecute, load_agent_executor, load_chat_planner
from langchain.callbacks import get_openai_callback
from langchain.agents import Tool, AgentOutputParser, tool
from langchain.prompts import StringPromptTemplate
from typing import List, Union
from langchain.schema import AgentAction, AgentFinish
from langchain.prompts import PromptTemplate
import streamlit as st
import re
import glob
from langchain.agents import initialize_agent
from langchain.agents import AgentType
from utils import load_model, getInstalledModels

# ########################### TOOLS ###########################

@tool
def ilo_definitions(query: str) -> str:
    """Provides definitions for Intended Learning Outcome and Learning Objective"""
    return open("./rateILO_prompts/definitions.txt").read()

@tool
def ilo_aspects(query: str) -> str:
    """Provides information about how an Intended Learning Outcome should be formulated, what should be respected and what should be avoided"""
    return open("./rateILO_prompts/ILO_aspects.txt").read()

@tool
def ilo_examples(query: str) -> str:
    """Provides examples of excellent and bad Intended Learning Outcomes, as well as Learning Objectives"""
    return open("./rateILO_prompts/ILO_examples.txt").read()

tools = [ilo_definitions, ilo_aspects, ilo_examples]

# ########################### AGENT PROMPT ###########################

template = """
    The following string is to be evaluated and possibly improved. It is most probably an Intended Learning Outcome, but might also be a Learning Objective or none of these.
    `{ilo}`

    1. Find out whether the string is an Intended Learning Outcome, a Learning Objective or none of these.
        1.1: Refuse to continue if the string is neither a Intended Learning Outcome nor a Learning Objective.
        1.2: If the string is a Learning Objective, rephrase it to become an Intended Learning Outcome.
    2. Find good and bad examples for Intended Learning Outcomes and try to rate the provided one.
    3. Evaluate the provided Intended Learning Outcome regarding aspects and guidelines for such.
    4. Rate the quality of the provided Intended Learning Outcome on a scale from 1 (worst) to 10 (best).
    5. Provide a concise list of possible improvements for the provided Intended Learning Outcome. Make sure that all listed improvements can be applied to it. Do not list anything that the provided Intended Learning Outcome already satisfies.
       
    Do not rephrase the Intended Learning Outcome yet.

    Yout got the following tools to operate with:
    {tools}
    If one of the tools isn't helpful, try to think on your own.
    Use each tool only once!
    
    Final Answer: The provided string is a ... . This is my evalutation:
    Quality: x/10

    Possible Improvements:
    - ...

    {agent_scratchpad}
"""

class CustomPromptTemplate(StringPromptTemplate):
    # The template to use
    template: str
    # The list of tools available
    tools: List[Tool]

    def format(self, **kwargs) -> str:
        # Get the intermediate steps (AgentAction, Observation tuples)
        # Format them in a particular way
        print(kwargs)
        intermediate_steps = kwargs.pop("intermediate_steps")
        thoughts = ""
        for action, observation in intermediate_steps:
            thoughts += action.log
            thoughts += f"\nObservation: {observation}\nThought: "
        print(thoughts)
        # Set the agent_scratchpad variable to that value
        kwargs["agent_scratchpad"] = thoughts
        # Create a tools variable from the list of tools provided
        kwargs["tools"] = "\n".join([f"{tool.name}: {tool.description}" for tool in self.tools])
        # Create a list of tool names for the tools provided
        kwargs["tool_names"] = ", ".join([tool.name for tool in self.tools])
        return self.template.format(**kwargs)

prompt = CustomPromptTemplate(
    template=template,
    tools=tools,
    # This omits the `agent_scratchpad`, `tools`, and `tool_names` variables because those are generated dynamically
    # This includes the `intermediate_steps` variable because that is needed
    input_variables=["ilo", "intermediate_steps"]
)

# ########################### OUTPUT PARSER ###########################

class CustomOutputParser(AgentOutputParser):
    def parse(self, llm_output: str) -> Union[AgentAction, AgentFinish]:
        # Check if agent should finish
        if "Final Answer:" in llm_output:
            return AgentFinish(
                # Return values is generally always a dictionary with a single `output` key
                # It is not recommended to try anything else at the moment :)
                return_values={"output": llm_output.split("Final Answer:")[-1].strip()},
                log=llm_output,
            )
        if "i should" in llm_output.lower():
            return
        # Parse out the action and action input
        regex = r"Action\s*\d*\s*:(.*?)\nAction\s*\d*\s*Input\s*\d*\s*:[\s]*(.*)"
        match = re.search(regex, llm_output, re.DOTALL)
        if not match:
            raise ValueError(f"Could not parse LLM output: `{llm_output}`")
        action = match.group(1).strip()
        action_input = match.group(2)
        # Return the action and action input
        return AgentAction(tool=action, tool_input=action_input.strip(" ").strip('"'), log=llm_output)

output_parser = CustomOutputParser()

# ########################### STREAMLIT APP ###########################

st.set_page_config(layout="wide")
st.title('Intended Learning Outcome evaluation using LLMs')
models = getInstalledModels()
chosenModel = st.selectbox('Model', (models), format_func=lambda path: re.split('/', path)[-1])
if chosenModel != '':
    llm = load_model(chosenModel)

# ########################### AGENT SETUP ###########################
# planner = load_chat_planner(llm)
# executor = load_agent_executor(llm, tools, verbose=True)
# agent = PlanAndExecute(planner=planner, executor=executor, verbose=True)

agent = initialize_agent(
    tools, 
    llm, 
    agent=AgentType.CHAT_ZERO_SHOT_REACT_DESCRIPTION, 
    verbose=True, 
    return_intermediate_steps=True,
    max_iterations=10
    )

if 'task' not in st.session_state:
    st.session_state['task'] = None

ilo = st.text_area("Intended Learning Outcome")
if ilo != "":
    with get_openai_callback() as cb:
        st.session_state.task = agent(prompt.format(ilo=ilo, intermediate_steps=""))
    st.write(f"Total Cost (USD): ${cb.total_cost}")

if st.session_state.task:
    # st.write(st.session_state.task.output)
    st.write(st.session_state.task)
