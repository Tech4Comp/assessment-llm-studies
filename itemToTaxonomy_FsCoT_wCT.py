import os
import streamlit as st
import json
import sys
from langchain.prompts import PromptTemplate
from langchain.callbacks import get_openai_callback
from pydantic import BaseModel, Field
from typing import List
import re
import glob
from utils import createConversation, JSONOutputParser, removeIdenticalElements, load_model, writeItemToSite, formatItemForPrompt, getInstalledModels

st.set_page_config(layout="wide")

# ########################### PREPARE PROMPTs ###########################

ak_context = open("./itemToTaxonomy_prompts/ak_context.txt").read()

# from https://learnlab.org/wiki/images/9/9f/Krathwohl_Bloom%27s_taxonomy_revised.pdf
taxonomy_description = open("./itemToTaxonomy_prompts/taxonomy_description.txt").read()

general_instructions = """
## Instructions ##
You will be asked to analyse a given assessment item and classify it according to the taxonomy matrix.

For the considered assessment item:
1. Analyse the item regarding to the required cognitive processes of the item type.
2. Determine the task to be performed by the tested person.
3. For the identified task, identifiy cognitive processes related to specific knowledge, that have to be used to solve the assessment item.
4. Output the identified cognitive processes and related knowledge in the specified format.

We will do this step by step.
"""

# different from txt file
example = """
## Example ##
### Assessment Item ###
Typ: Single Choice - Eine Antwortoption ist richtig
Titel: Disjunktheit
Vignette: Gegeben sind die Mengen A = {1, 2, 3, 4} und B = {3, 4, 5, 6}.
Aufgabe: Sind die Mengen A und B disjunkt?
Antwortoptionen:
1: Ja (falsch)
2: Nein (richtig)

Kontext: Mengenlehre wurde im Unterricht behandelt. Der Begriff Disjunktheit wurde definiert und an einem Beispiel wurde gezeigt was es heißt, wenn zwei Mengen Disjunkt sind. Die Vorgehensweise zur Feststellung der Disjunkheit wurde behandelt.

### Step 1 ###
The assessment item is a single choice item. Thus the tested person is requested to 1: understand the task, 2: execute the task, 3: analyse the given options, 4: evaluate the given options with resprect to the task result, and 5: select the correct option.

Cognitive processes related to the item type: understand (1), analyse (2), evaluate (3,4)
### Step 2 ###
The task is to determine whether two sets are disjoint, which results in a binary yes/no answer.
### Step 3 ###
To execute the identified task, the tested person has to remember the concept of disjointness in set theory (A). Then the tested person has to remember the procedure to determine disjointness (B). Then, the tested person has to apply the algorithm on the provided sets to determine whether the sets are disjoint (C).

Cognitive processes and knowledge related to the task: remember/conceptual (A), remember/procedural (B), apply/procedural (C)

Finally, the tested person is required to evaluate the given answer options, choosing the correct one.
### Step 4 ###
[{
    "processDimension": "remember",
    "knowledgeDimension": "conceptual"
},
{
    "processDimension": "remember",
    "knowledgeDimension": "procedural"
},
{
    "processDimension": "apply",
    "knowledgeDimension": "procedural"
}]
"""

task1 = """
## Assessment Item ##
{item}

## Step 1 ##
Analyse the above assessment item written in German regarding to the required cognitive processes of the item type.
Complete step 1 only.
"""
task2 = """
## Step 2 ##
For the above assessment item, determine the task to be performed.
Complete step 2 only.
"""
task3 = """
## Step 3 ##
For the identified task of the above assessment item, identifiy cognitive processes related to specific knowledge, that have to be used to solve the assessment item. 
Complete step 3 only.
"""
output_formatter = """
## Step 4 ##
""" + open("./itemToTaxonomy_prompts/output_formatter.txt").read() + """

Complete step 4 only.
"""

classify_ak_prompt = PromptTemplate(
    template=task1,
    input_variables=["item"]
)

json_parser = JSONOutputParser()

# ########################### Additional Functions ###########################

@st.cache_data
def processItem(_conversation, _memory, item) -> list:
    text = formatItemForPrompt(item)
    thoughts = []
    thoughts.append(conversation.predict(input=ak_context + taxonomy_description + general_instructions + example + classify_ak_prompt.format(item=text)))
    # print(thoughts[0])
    thoughts.append(conversation.predict(input=task2))
    # print(thoughts[1])
    thoughts.append(conversation.predict(input=task3))
    # print(thoughts[2])
    thoughts.append(conversation.predict(input=output_formatter))
    # print(thoughts[3])
    return thoughts

# ########################### CREATE STREAMLIT-APP ###########################

st.title('Item taxonomy classification using LLMs')
st.warning("This is a research prototype and might produce misleading results. Check everything thoroughly!")

models = getInstalledModels()
chosenModel = st.selectbox('Model', (models), format_func=lambda path: re.split('/', path)[-1])
if chosenModel != '':
    llm = load_model(chosenModel)

conversation, memory = createConversation(llm)

if 'costs' not in st.session_state:
    st.session_state['costs'] = 0

uploaded_file = st.file_uploader('Items', type=['json','jsonld'], help='A file that contains one or more items')

items: list = None
if uploaded_file is not None:
    items = json.load(uploaded_file)

processNumberOfItems = st.number_input("Stop after x Items:", min_value=1, value=1, step=1)


st.write("Taxonomy: Anderson & Krathwohl (24 levels)")

if items is not None and len(items) > 0:
    with st.spinner('Processing...'):
        with get_openai_callback() as cb:
            for index, item in enumerate(items['@graph']):
                writeItemToSite(item)
                thoughts = processItem(conversation, memory, item)
                output = thoughts.pop()
                if item.get('annotations') is None:
                    item['annotations'] = {}
                try:
                    extracted_json = json.loads("" + json_parser.parse(output))
                    item['annotations']['akLLM'] = extracted_json
                    st.json(extracted_json)
                    with st.expander("Thoughts:"):
                        item['annotations']['akLLMThoughts'] = thoughts
                        st.write('\n\n'.join(thoughts))
                    st.divider()
                except ValueError as error:
                    st.write("Classification not possible: " + error)
                finally:
                    memory.clear()

                if index + 1 == processNumberOfItems:
                    break
        print(cb)
        st.session_state.costs = st.session_state.costs + cb.total_cost
    st.success('Done!')
    with open('result.json', 'w', encoding='utf-8') as f:
        json.dump(items, f, ensure_ascii=False, indent=2)

if st.session_state.costs:
    st.write("Total Cost (USD Cent): ¢{:.4f}".format((st.session_state.costs * 100)))