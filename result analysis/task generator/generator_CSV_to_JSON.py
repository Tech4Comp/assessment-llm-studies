import csv
import sys
import json

def starToNumber(text):
  return text.count("⭐")

def GermanToEnglish(text: str):
  text = text.strip().lower()
  if text == "erinnern":
    return "remember"
  elif text == "verstehen":
    return "understand"
  elif text == "anwenden":
    return "apply"
  elif text == "analysieren":
    return "analyse"
  elif text == "evaluieren":
    return "evaluate"
  elif text == "erschaffen":
    return "create"
  elif text == "faktenwissen":
    return "factual"
  elif text == "konzeptionelles wissen" or text == "konzeptionelles_wissen":
    return "conceptual"
  elif text == "prozedurales wissen" or text == "prozedurales_wissen":
    return "procedural"
  elif text == "metakognitives wissen" or text == "metakognitives_wissen":
    return "metacognitive"

items = {"@graph" : []}

def initialItemsCSV(file, items, key = "akManual"):
  with open(file, 'r', encoding="utf8") as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
      if reader.line_num > 3:
        print(reader.line_num)
        item = {
          "@id": row[0],
          "@type": "Free Text",
          "task": row[1],
          "answers": [
            {
              "@type": "LLMSolution",
              "text": row[2],
              "points": 1
            }
          ],
          "annotations": {
            key: [
              {
                "comment": "Rater from " + file,
                "processDimension": GermanToEnglish(row[4]),
                "knowledgeDimension": GermanToEnglish(row[5])
              }
            ],
            "topics": [row[3]],
            "concept_appropriateness": [starToNumber(row[6])],
            "task_completeness": [starToNumber(row[7])],
            "task_correctness": [starToNumber(row[8])],
            # "didactial_appropriateness": [starToNumber(row[10])],
            "solution_completeness": [starToNumber(row[9])],
            "solution_correctness": [starToNumber(row[10])],
            "comment": [row[11]]
          }
        }
        items["@graph"].append(item)

def addToItemsCSV(file, items, key = "akManual", create = False):
  with open(file, 'r', encoding="utf8") as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
      if reader.line_num > 3:
        for item in items["@graph"]:
          if item["@id"] == row[0]:
            if create:
              item["annotations"][key] = []
            item["annotations"][key].append(
              {
                "comment": "Rater from " + file,
                "processDimension": GermanToEnglish(row[4]),
                "knowledgeDimension": GermanToEnglish(row[5])
              }
            )
            item["annotations"]["topics"].append(row[3])
            item["annotations"]["concept_appropriateness"].append(starToNumber(row[6]))
            item["annotations"]["task_completeness"].append(starToNumber(row[7]))
            item["annotations"]["task_correctness"].append(starToNumber(row[8]))
            # item["annotations"]["didactial_appropriateness"].append(starToNumber(row[10]))
            item["annotations"]["solution_completeness"].append(starToNumber(row[9]))
            item["annotations"]["solution_correctness"].append(starToNumber(row[10]))
            item["annotations"]["comment"].append(row[11])

def addToItemsGenerator(file, items):
  with open(file, 'r', encoding="utf8") as jsonfile:
    data = json.load(jsonfile)
    for task in data:
      for item in items["@graph"]:
          if item["@id"] == task['id']:
            item["annotations"]["akTaskGenerator"] = []
            item["annotations"]["akTaskGenerator"].append(
              {
                "processDimension": GermanToEnglish(task['process_dimension']),
                "knowledgeDimension": GermanToEnglish(task['knowledge_dimension'])
              }
            )

if len(sys.argv) <= 1:
  print("Too few CSV files provided")
  sys.exit()

if sys.argv[1] not in ["-g", "-i"]:
  print('Use like:')
  print('For inter-rater comparisons:')
  print('python ./generator_CSV_to_JSON.py -i rater1.csv rater2.csv output.json')
  print()
  print('For rater-generator comparisons:')
  print('python ./generator_CSV_to_JSON.py -g rater1.csv ... generator.json output.json')

elif sys.argv [1] == "-i":
  print('Creating a file that compares human values against human values')
  if len(sys.argv) == 5:
    initialItemsCSV(sys.argv[2], items, "Rater_A")
    addToItemsCSV(sys.argv[3], items, "Rater_B", True)

elif sys.argv [1] == "-g":
  print('Creating a file that compares generated values against human values')
  print(sys.argv)
  if len(sys.argv) >= 4:
    initialItemsCSV(sys.argv[2], items)

  for index in range(2, len(sys.argv) - 1):
    if index + 1 < len(sys.argv) - 1:
      if not sys.argv[index + 1].endswith(".json"):
        addToItemsCSV(sys.argv[index + 1], items)
      else:
        addToItemsGenerator(sys.argv[index + 1], items)

with open(sys.argv[-1], 'w', encoding='utf-8') as f:
            json.dump(items, f, ensure_ascii=False, indent=2)
