import json
import numpy as np
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, classification_report
import streamlit as st
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

st.set_page_config(layout="wide")

def preProcess(items):
    '''uniformize the data structure'''
    for item in items:
        if item["annotations"].get("bloom", None) is not None:
            item["annotations"][comparison_key] = item["annotations"]["bloom"]
            del item["annotations"]["bloom"]
        if item["annotations"].get(analyse_key, None) is not None:
            for value in item["annotations"][analyse_key]:
                # print(item["annotations"].get(analyse_key, None))
                removeURLPrefix(value)
        if item["annotations"].get(comparison_key, None) is not None:
            for value in item["annotations"][comparison_key]:
                removeURLPrefix(value)

def addURLPrefix(value):
    if not value["processDimension"].startswith("http"):
        value["processDimension"] = "http://tech4comp/eal/" + value["processDimension"]
    if not value["knowledgeDimension"].startswith("http"):
        value["knowledgeDimension"] = "http://tech4comp/eal/" + value["knowledgeDimension"]

def removeURLPrefix(value):
    if value["processDimension"].startswith("http"):
        value["processDimension"] = value["processDimension"].removeprefix("http://tech4comp/eal/")
    if value["knowledgeDimension"].startswith("http"):
        value["knowledgeDimension"] = value["knowledgeDimension"].removeprefix("http://tech4comp/eal/")

def highestDimension(labels: list[str], dims: list[str]) -> str :
    '''returns the highest dimension'''
    try:
        dim_indexes: list[int] = sorted(list(map(lambda x : labels.index(x), dims)))
        return labels[dim_indexes[-1]]
    except:
        return ""

def inDimension(dimA: list[str], dimB: list[str]) -> str:
    '''returns a value that exists in both lists'''
    try:
        return sorted(list(set(dimA) & set(dimB)))[-1]
    except:
        return None

def dimToNumber(labels, dim) -> int:
    '''returns the numerical value of the dimension, e.g. remember -> 1'''
    if(dim in labels):
        return labels.index(dim) + 1
    else:
        return 0

def numberToDim(labels, dim) -> int:
    '''returns the string value of the numerical dimension representation, e.g. 1 -> remember'''
    if dim >= 1 and dim <= len(labels):
        return labels[dim - 1]
    else:
        return 'unknown'

def fillDimRange(labels: list[str], dims: list[str]):
    dimNumbers = [dimToNumber(labels,dim) for dim in dims]
    filled = list(range(min(dimNumbers), max(dimNumbers) + 1))
    return [numberToDim(labels,dim) for dim in filled]

def printConfusionMatrix(header: str, truth: list, prediction: list, labels: list) :
    '''print a confusion matrix and report to the streamlit app'''
    st.header(header)
    ConfMatrix = confusion_matrix(truth, prediction, labels=labels) # normalize='all'
    disp = ConfusionMatrixDisplay(confusion_matrix=ConfMatrix, display_labels=labels)
    disp.plot(xticks_rotation = 'vertical')
    plt.xlabel(analyse_key)
    plt.ylabel(comparison_key)
    st.pyplot(fig=disp.figure_, use_container_width=False)
    st.text("Report: \n" + classification_report(truth, prediction, labels=labels))

def heatmap_matrix(items, labelsPD, labelsKD, key):
    matrix = np.zeros((6, 5))
    for item in items:
        if item["annotations"].get(key, None) is not None:
            for dim in item["annotations"].get(key):
                line = labelsPD.index(dim["processDimension"])
                column = (["unknown"] + labelsKD).index(dim["knowledgeDimension"])
                matrix[line][column] += 1
    return matrix

def printHeatMap(matrix, labelsPD, labelsKD, title):
    fig, ax = plt.subplots(figsize=(6, 5))
    ax = sns.heatmap(pd.DataFrame(matrix, index=labelsPD, columns=["unknown"] + labelsKD), vmin=0, vmax=300, annot=True)
    plt.title(title)
    st.pyplot(fig=fig, use_container_width=False)

# ########################### CREATE STREAMLIT-APP ###########################

st.title('Item taxonomy classification result rating')
st.warning("This is a research prototype and might produce misleading results. Check everything thoroughly!")

file = st.file_uploader('Rated Items', type=['json','jsonld'], help='A file that contains one or more rated items')
st.write("Analyseoptionen:")
analyse_key = st.selectbox("Zu analysierendes Datum", ["akTaskGenerator", "akLLM", "akManual", "Rater_A", "Rater_B"])
comparison_key = st.selectbox("Vergleichdatum", ["akManual", "akTaskGenerator", "akLLM", "Rater_A", "Rater_B"])
options = [None, "Heatmap", "Confusion-Höchste", "Confusion-Enthalten", "Confusion-Enthalten-Range", "Confusion-24er-enthalten", "Confusion-24er-höchste"]
analyse_method = st.selectbox("Analyseoptionen", options)

st.text("""
Heatmap: Diese Analyse stellt alle Grundwahrheiten und Vorhersagen als Heatmap dar
Confusion-Höchste: Diese Analyse vergleicht, ob die höchste Vorhersage-Dimension der höchsten Wahrheits-Dimension entspricht
Confusion-Enthalten: Diese Analyse vergleicht, ob überhaupt eine der vorhergesagten Dimensionen der Wahrheit entspricht
Confusion-Enthalten-Range: Diese Analyse vergleicht, ob überhaupt eine der vorhergesagten Dimensionen der Wahrheit entspricht, wobei die Range zw. maximaler und minimaler Vorhersage gefüllt wird
Confusion-24er-enthalten: Diese Analyse kombiniert die Prozess- & Wissensdimension und vergleicht, ob eine PD/KD Kombination in der Wahrheit enthalten ist
Confusion-24er-höchste: Diese Analyse kombiniert die Prozess- & Wissensdimension und vergleicht, ob die höchste ((PD/6 + KD/4) PD/KD Kombination der höchsten Wahrheit entspricht
""")

items: dict = None
if file is not None:
    items = json.load(file)

if items is not None and len(items) > 0 and analyse_method is not None:
    # potential keys: annotations: [ "akManual", "akLLM", "bloom" ]
    preProcess(items["@graph"])
    truthPD = []
    predictionPD = []
    labelsPD = ["remember", "understand", "apply", "analyse", "evaluate", "create"]
    truthKD = []
    predictionKD = []
    labelsKD = ["factual", "conceptual", "procedural", "metacognitive"]
    finished = False
    if "Heatmap" in analyse_method:
        plt.clf()
        if analyse_key == "Rater_A":
            heading = items['@graph'][0]['annotations'][analyse_key][0]['RaterName']
        else:
            heading = comparison_key
        if comparison_key == "Rater_B":
            heading2 = items['@graph'][0]['annotations'][comparison_key][0]['RaterName']
        else:
            heading2 = comparison_key
        printHeatMap(heatmap_matrix(items['@graph'], labelsPD, labelsKD, analyse_key), labelsPD, labelsKD, heading)
        printHeatMap(heatmap_matrix(items['@graph'], labelsPD, labelsKD, comparison_key), labelsPD, labelsKD, heading2)
    if analyse_method == "Confusion-Höchste":
        plt.clf()
        for item in items['@graph']:
            if item["annotations"].get(analyse_key, None) is not None and item["annotations"].get(comparison_key, None) is not None:
                truthPD.append(highestDimension(labelsPD, list(dim["processDimension"] for dim in item["annotations"][comparison_key])))
                predictionPD.append(highestDimension(labelsPD, list(dim["processDimension"] for dim in item["annotations"][analyse_key])))
                truthKD.append(highestDimension(labelsKD, list(dim["knowledgeDimension"] for dim in item["annotations"][comparison_key])))
                predictionKD.append(highestDimension(labelsKD, list(dim["knowledgeDimension"] for dim in item["annotations"][analyse_key])))
        finished = True
    if analyse_method == "Confusion-Enthalten":
        plt.clf()
        for item in items['@graph']:
            if item["annotations"].get(analyse_key, None) is not None and item["annotations"].get(comparison_key, None) is not None:
                # list[process/knowledge]
                pdManual: list[str] = list(dim["processDimension"] for dim in item["annotations"][comparison_key])
                pdPredict: list[str] = list(dim["processDimension"] for dim in item["annotations"][analyse_key])
                tmp: str = inDimension(pdManual, pdPredict)
                if pdManual and pdPredict: # lists not empty
                    truthPD.append(tmp if tmp is not None else pdManual[-1])
                    predictionPD.append(tmp if tmp is not None else pdPredict[-1])
                
                kdManual: list[str] = list(dim["knowledgeDimension"] for dim in item["annotations"][comparison_key])
                kdPredict: list[str] = list(dim["knowledgeDimension"] for dim in item["annotations"][analyse_key])
                tmp: str = inDimension(kdManual, kdPredict)
                if kdManual and kdPredict: # lists not empty
                    truthKD.append(tmp if tmp is not None else kdManual[-1])
                    predictionKD.append(tmp if tmp is not None else kdPredict[-1])
        finished = True
    if analyse_method == "Confusion-Enthalten-Range":
        plt.clf()
        for item in items['@graph']:
            if item["annotations"].get(analyse_key, None) is not None and item["annotations"].get(comparison_key, None) is not None:
                # list[process/knowledge]
                pdManual: list[str] = list(dim["processDimension"] for dim in item["annotations"][comparison_key])
                pdManual = fillDimRange(labelsPD, pdManual)
                pdPredict: list[str] = list(dim["processDimension"] for dim in item["annotations"][analyse_key])
                if pdManual and pdPredict: # lists not empty
                    pdPredict = fillDimRange(labelsPD, pdPredict)
                    tmp: str = inDimension(pdManual, pdPredict)
                    truthPD.append(tmp if tmp is not None else pdManual[-1])
                    predictionPD.append(tmp if tmp is not None else pdPredict[-1])

                kdManual: list[str] = list(dim["knowledgeDimension"] for dim in item["annotations"][comparison_key])
                kdManual = fillDimRange(labelsKD, kdManual)
                kdPredict: list[str] = list(dim["knowledgeDimension"] for dim in item["annotations"][analyse_key])
                if kdManual and kdPredict: # lists not empty
                    kdPredict = fillDimRange(labelsKD, kdPredict)
                    tmp: str = inDimension(kdManual, kdPredict)
                    truthKD.append(tmp if tmp is not None else kdManual[-1])
                    predictionKD.append(tmp if tmp is not None else kdPredict[-1])
        finished = True
    if analyse_method == "Confusion-24er-enthalten":
        plt.clf()
        for item in items['@graph']:
            if item["annotations"].get(analyse_key, None) is not None and item["annotations"].get(comparison_key, None) is not None:
                # set[process/knowledge]
                manual: set[str] = set(dim["processDimension"] + "/" + dim["knowledgeDimension"] for dim in item["annotations"][comparison_key])
                predict: set[str] = set(dim["processDimension"] + "/" + dim["knowledgeDimension"] for dim in item["annotations"][analyse_key])
                tmp: str = inDimension(manual, predict)
                # TODO not sure adding "" is correct
                truthPD.append(tmp if tmp is not None else (manual.pop() if len(manual) > 0 else ""))
                predictionPD.append(tmp if tmp is not None else (predict.pop() if len(predict) > 0 else ""))
        finished = True
    if analyse_method == "Confusion-24er-höchste":
        plt.clf()
        for item in items['@graph']:
            if item["annotations"].get(analyse_key, None) is not None and item["annotations"].get(comparison_key, None) is not None:
                # dict[0-1 : process/knowledge]
                manual: dict[float, str] = dict([
                    (dimToNumber(labelsPD, dim["processDimension"])/6 + dimToNumber(labelsKD, dim["knowledgeDimension"])/4)/2,
                    dim["processDimension"] + "/" + dim["knowledgeDimension"]]
                    for dim in item["annotations"][comparison_key])
                predict: dict[float, str] = dict([
                    (dimToNumber(labelsPD, dim["processDimension"])/6 + dimToNumber(labelsKD, dim["knowledgeDimension"])/4)/2,
                    dim["processDimension"] + "/" + dim["knowledgeDimension"]]
                    for dim in item["annotations"][analyse_key])
                manualSorted = sorted(list(manual.keys()))
                predictSorted = sorted(list(predict.keys()))
                # TODO not sure adding "" is correct
                highestManual: str = manual[manualSorted[-1]] if len(manualSorted) > 0 else ""
                highestPredict: str = predict[predictSorted[-1]] if len(predictSorted) > 0 else ""
                # highestPredict: str = predict[predictSorted[-1]]
                truthPD.append(highestManual)
                predictionPD.append(highestPredict)
        finished = True

    if(finished):
        if analyse_method.startswith("Confusion-24er"):
            labels = []
            for i in labelsPD:
                for s in labelsKD:
                    labels.append(i + "/" + s)
            printConfusionMatrix("", truthPD, predictionPD, labels)
        else:
            printConfusionMatrix("Process-Dimension", truthPD, predictionPD, labelsPD)
            st.divider()
            printConfusionMatrix("Knowledge-Dimension:", truthKD, predictionKD, labelsKD)