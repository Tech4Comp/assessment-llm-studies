import unittest
from rateResults_itemToTaxonomy import highestDimension, inDimension

class TestHighestDimension(unittest.TestCase): 
    def test_highest_dimension(self): 
        labels = ['A', 'B', 'C', 'D'] 
        dims = ['B', 'D'] 
        self.assertEqual(highestDimension(labels, dims), 'D')

        labels = ['X', 'Y', 'Z']
        dims = ['Y']
        self.assertEqual(highestDimension(labels, dims), 'Y')

        labels = ['1', '2', '3', '4']
        dims = ['1', '2', '3']
        self.assertEqual(highestDimension(labels, dims), '3')

        labels = ['A', 'B', 'C']
        dims = ['D']
        self.assertEqual(highestDimension(labels, dims), "")

class TestInDimension(unittest.TestCase): 
    def test_in_dimension(self): 
        labels = ['A', 'B', 'C', 'D'] 
        dims = ['B', 'D'] 
        self.assertEqual(inDimension(labels, dims), 'D')

        labels = ['X', 'Y', 'Z']
        dims = ['Y']
        self.assertEqual(inDimension(labels, dims), 'Y')

        labels = ['1', '2', '3', '4']
        dims = ['1', '2', '3']
        self.assertEqual(inDimension(labels, dims), '3')

        labels = ['A', 'B', 'C']
        dims = ['D']
        self.assertEqual(inDimension(labels, dims), None)


if __name__ == '__main__':
    unittest.main()