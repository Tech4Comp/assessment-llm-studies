# Assessment LLM Studies

Important Notice: Due to used dependencies, the project needs to be built and run with python<3.12 (e.g. 3.11)

Run individual apps with `streamlit run ./<APP>.py`

Individual studies are named according to the following prompting schemes:
- Zs: [Zero-shot](https://www.promptingguide.ai/techniques/zeroshot) - ask the question directly
- Fs: [Few-shot](https://www.promptingguide.ai/techniques/fewshot) - like Zs, but example(s) (task/question & answer) are provided
- CoT: [Chain of Thought Prompting](https://www.promptingguide.ai/techniques/cot) - manually break processing into steps and execute step by step
- KG: [Knowledge Generation](https://www.promptingguide.ai/techniques/knowledge) - let the LLM generate knowledge first, before answering the question/task
- SC: [Self-Consistency](https://www.promptingguide.ai/techniques/consistency)
- RAG: [Retrieval Augmented Generation](https://www.promptingguide.ai/techniques/rag) - retrieve knowledge from external sources
- ReAct: [Reason and Act](https://www.promptingguide.ai/techniques/react) - Reason about traces and decide for task-specific actions
- mA: multiple Agents - Utilize several conversations/agents to work on specific tasks
- prA: peer-review Agent - Utilize a second conversation/agent to review the information/thoughts of the first agent
- DiD: Describe question/task in detail first and think through it

## Classify Items with Anderson & Krathwohl (itemToTaxonomy)

Input items are read from a file, processed, and the result is saved into a new file

- naive: internal knowledge of the LLM only
- wC: with Context - brief description of context (bloom, anderson & krathwohl, name levels)
- wT: with Taxonomy - description of the different taxonomy levels for the cognitive process and knowledge domains
- wD: with Domain knowledge - retrieve relevant content & summarize to get what learners should know/be capable of
- wCu: with Curricula - retrieve relevant curricula data & summarize to get the course context and informal information of what learners should be capable of and know from a birds eye perspective

If using a wD variant, create a folder that starts with `domain_context_` in the root folder. The program will search for folders with this name and provide these as a dropdown to choose from.

## Rating and improving Intended Learning Outcomes (ILO) (rateLO)

Input is read from a text area, processed, and the result is displayed on the webpage

- wD: with Definitions - Definitions of ILO and Learning Objectives
- wA: with Aspects - aspects and guidelines for good ILOs
- wE: with Examples - examples of good and bad ILOs

