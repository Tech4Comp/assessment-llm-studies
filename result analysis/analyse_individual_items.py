import json
import numpy as np
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, classification_report
import streamlit as st
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import functools
import re

st.set_page_config(layout="wide")
labelsPD = ["remember", "understand", "apply", "analyse", "evaluate", "create"]
labelsKD = ["factual", "conceptual", "procedural", "metacognitive"]

def dimToNumber(labels, dim) -> int:
    '''returns the numerical value of the dimension, e.g. remember -> 1'''
    if(dim in labels):
        return labels.index(dim) + 1
    else:
        return 0
    
def simplifyType(type):
  if type == "http://tech4comp/eal/MultipleChoiceItem":
    return "MC"
  if type == "http://tech4comp/eal/SingleChoiceItem":
    return "SC"
  if type == "http://tech4comp/eal/FreeTextItem":
    return "FT"
  if "/" in type:
    return type

def simplifyID(id):
  # split id at last occurance of / and take the latter part
    return id.rsplit('/', 1)[-1]

def buildData(item):
    process_dimensions = []
    knowledge_dimensions = []
    for rater_annotations in item["annotations"].values():
        for annotation in rater_annotations:
            print(annotation)
            process_dimensions.append(dimToNumber(labelsPD, annotation["processDimension"]))
            knowledge_dimensions.append(dimToNumber(labelsKD, annotation["knowledgeDimension"]))
    process_dimensions.sort()
    knowledge_dimensions.sort()
    print(process_dimensions, knowledge_dimensions)
    return {
        "id": simplifyID(item["@id"]),
        "type": simplifyType(item["@type"]),
        "processDimensions": process_dimensions,
        "knowledgeDimensions": knowledge_dimensions,
        }

def compressData(data):
    def mapEntry(entry):
        return {
        "id": entry["id"],
        "type": entry["type"],
        "pd_mean": np.mean(entry["processDimensions"]),
        "pd_median": np.median(entry["processDimensions"]),
        "pd_std": np.std(entry["processDimensions"]),
        "kd_mean": np.mean(entry["knowledgeDimensions"]),
        "kd_median": np.median(entry["knowledgeDimensions"]),
        "kd_std": np.std(entry["knowledgeDimensions"]),
        }
    return map(mapEntry, data)

def printTable(header: str, data) :
    '''print a sortable data frame'''
    st.write("""
    Concept Apropriatness: ca  
    Task Completeness: tcm  
    Task Correctness: tcr  
    Didactial Appropriateness: da  
    Solution Completeness: scm  
    Solution Correctness: scr  
    """)
    st.write("Values: Mean, std: Standard Deviation")
    st.header(header)
    st.data_editor(pd.DataFrame(compressData(data)), hide_index=True, )

# ########################### CREATE STREAMLIT-APP ###########################

st.title('Item generator classification result analysis')
st.warning("This is a research prototype and might produce misleading results. Check everything thoroughly!")

file = st.file_uploader('Rated Items', type=['json','jsonld'], help='A file that contains one or more rated items')

items: dict = None
if file is not None:
    items = json.load(file)

if items is not None and len(items) > 0:
    data = []
    for item in items["@graph"]:
        data.append(buildData(item))
    data = sorted(data, key=lambda k: int(k["id"].replace("-old", "").replace("test", "1")))
    printTable("Full data set", data)

    analyse_key = st.selectbox("Data to analyse", ["processDimensions", "knowledgeDimensions", "overall"])
    if analyse_key is not None:
        if analyse_key == "overall":
            df = pd.DataFrame(compressData(data))

            zu_entfernende_spalten = [spalte for spalte in df.columns if spalte.endswith('_std')]
            df = df.drop(zu_entfernende_spalten, axis=1)

            fig, ax = plt.subplots()
            sns.set(style="whitegrid")
            sns.boxplot(data=df, orient="v")

            # Abstand diagram auf 0.1 stellen
            #df2 = df.select_dtypes(include=[np.number])
            #tick_values = np.arange(df2.values.min(), df2.values.max() + 0.1, 0.1)
            #plt.yticks(tick_values)

            plt.title("Boxplot über alle Aufgaben")
            st.pyplot(fig=fig, use_container_width=False)
            plt.show()

            complete = df.mean()
            durchschnitts_df = df.groupby('concept').mean()
            durchschnitts_df["RATING"] = durchschnitts_df.mean(axis=1)
            st.write(durchschnitts_df)

            # ABSTAND DER BEWERTUNGEN BERECHNEN

            def pd_to_val(dim_str):
                dim_mapping = {
                    "remember": 0,
                    "understand": 1,
                    "apply": 2,
                    "analyse": 3,
                    "evaluate": 4,
                    "create": 5
                }
                return dim_mapping.get(dim_str, -1)  # -1 für unknown


            def kd_to_val(dim_str):
                dim2_mapping = {
                    "factual": 0,
                    "conceptual": 1,
                    "procedural": 2,
                    "metacognitive": 3
                }
                return dim2_mapping.get(dim_str, -1) # -1 für unknown


            data2 = []
            for item in items["@graph"]:
                pd_rater = []
                kd_rater = []
                for entry in item["annotations"]["akManual"]:
                    pd_rater.append(pd_to_val(entry["processDimension"]))
                    kd_rater.append(kd_to_val(entry["knowledgeDimension"]))
                data2.append({
                    "id": item["@id"],
                    "concept": item["annotations"]["topics"][0],
                    "pd_gen": pd_to_val(item["annotations"]["akTaskGenerator"][0]["processDimension"]),
                    "kd_gen": kd_to_val(item["annotations"]["akTaskGenerator"][0]["knowledgeDimension"]),
                    "pd_rater": np.mean(pd_rater),
                    "kd_rater": np.mean(kd_rater)
                })

            df2 = pd.DataFrame(data2)
            df2["pd_diff"] = abs(df2['pd_gen'] - df2['pd_rater'])
            df2["kd_diff"] = abs(df2['kd_gen'] - df2['kd_rater'])

            # Normalisieren
            df2['pd_diff'] = (df2['pd_diff'] -  df2['pd_diff'].min()) / (df2['pd_diff'].max() -  df2['pd_diff'].min())
            df2['kd_diff'] = (df2['kd_diff'] - df2['kd_diff'].min()) / (df2['kd_diff'].max() - df2['kd_diff'].min())

            st.write(df2)
            #durchschnitts_df = df2.mean()
            durchschnitts_df = df2.groupby('concept').mean()
            st.write(durchschnitts_df)

        else:
            sns.set_theme(style="ticks")
            fig, ax = plt.subplots(figsize=(5, 40))

            print(sns.load_dataset("titanic"))
            def mapEntry(entry):
                data = []
                data.append({
                    "id": entry["id"],
                    "value": entry[analyse_key]
                })
                return data
            df = map(mapEntry, data)
            concatinated = merged_list = [item for sublist in df for item in sublist]
            expanded = []
            for entry in concatinated:
                for value in entry["value"]:
                    expanded.append({
                        "id": entry["id"],
                        "value": value
                    })

            if analyse_key == "processDimensions":
                expanded.append({
                    "id": "test",
                    "value": 6
                })
                plt.title("By Process Dimension")
            else:
                expanded.append({
                    "id": "test",
                    "value": 4
                })
                plt.title("By Knowledge Dimension")

            # sort expanded by id and modify id to remove any non number characters
            
            expanded = sorted(expanded, key=lambda k: int(k["id"].replace("-old", "").replace("test", "1")))
            
            print(pd.DataFrame(expanded))
            sns.boxplot(x="value", y="id", data=pd.DataFrame(expanded), medianprops={"color": "r", "linewidth": 1}, width=.6)

            ax.xaxis.grid(True)
            ax.set(ylabel="")
            sns.despine(trim=True, left=True)
            st.pyplot(fig=fig, use_container_width=False)