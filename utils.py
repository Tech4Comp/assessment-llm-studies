from langchain.memory import ConversationBufferMemory
from langchain.chains import ConversationChain, LLMChain
from langchain.prompts import PromptTemplate
from langchain.schema.language_model import BaseLanguageModel
from langchain.chat_models import ChatOpenAI
from langchain.llms import GPT4All
from keys import openAPI as apikey
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain.chains import RetrievalQA
from langchain.document_loaders import PyPDFLoader, DirectoryLoader
from langchain.embeddings import HuggingFaceEmbeddings
from langchain.vectorstores import FAISS
import streamlit as st
import bs4
from jsondiff import diff
import json
import glob
import os
import jsonschema
from jsonschema import validate
from pywinauto.fuzzydict import FuzzyDict
from contextlib import suppress

openAIModels = {"GPT-3.5-turbo": "gpt-3.5-turbo",
                "GPT-4-Omni-Mini": "gpt-4o-mini",
                "GPT-4-Omni": "gpt-4o",
                "GPT-4-turbo": "gpt-4-turbo",
                "GPT-4": "gpt-4"}

@st.cache_data
def load_model(path, temperature = 0) -> BaseLanguageModel:
    if (path.startswith("GPT-")):
        model = ChatOpenAI(temperature=temperature, openai_api_key=apikey, model_name=openAIModels.get(path))
    else:
        callbacks = [StreamingStdOutCallbackHandler()]
        model = GPT4All(model=path, n_threads=15, callbacks=callbacks, verbose=True)
    return model

def createConversation(llm: BaseLanguageModel, verbosity = False, enableMemory = True, systemTemplate = None) -> LLMChain:
    memory = None
    conversation = None
    if enableMemory:
        if systemTemplate is None:
            systemTemplate = open("./itemToTaxonomy_prompts/system_template.txt").read()
        memory = ConversationBufferMemory(human_prefix="Program", memory_key="history")
        conversation = ConversationChain(
            llm=llm,
            memory=memory,
            verbose=verbosity,
            prompt=PromptTemplate(input_variables = ['history', 'input'], template = systemTemplate)
        )
    else:
        # print("teste")
        if systemTemplate is None:
            systemTemplate = open("./itemToTaxonomy_prompts/system_template_no_history.txt").read()
        conversation = LLMChain(
            llm=llm,
            verbose=verbosity,
            prompt=PromptTemplate.from_template(systemTemplate),
        )
    return conversation, memory

class JSONOutputParser():
    def parse(self, llm_output: str) -> str:
        if "[" in llm_output:
            return "[" + (llm_output.split('[',1)[-1]).rsplit(']',1)[0] + "]"
        elif "{" in llm_output:
            return "[{" + (llm_output.split('{',1)[-1]).rsplit('}',1)[0] + "}]"
        else: 
            raise ValueError(f"No JSON included: `{llm_output}`")

    def get_format_instructions() -> str:
        return """
            Please answer with json only, according to the following template, using lower case words:
            [{{
                "knowledgeDimension": "",
                "processDimension": ""
            }}]
        """

def parseType(type: str) -> str:
    type_dict = {
        "http://tech4comp/eal/SingleChoiceItem": "Single Choice - Eine Antwortoption ist richtig",
        "http://tech4comp/eal/MultipleChoiceItem": "Multiple Choice - Eine oder mehrere Antwortoptionen sind richtig",
        "http://tech4comp/eal/FreeTextItem": "Free Text - Antwort in Form eines individuellen Textes",
        "http://tech4comp/eal/ArrangementItem": "Arrangement - Anordnung der zur Verfügung gestellten Optionen in richtiger Reihenfolge",
        "http://tech4comp/eal/AssignmentItem": "Assignment - Zuordnung der zur Verfügung gestellten Optionen zu einer vorgegebenen Liste",
    }
    if type in type_dict:
        return type_dict[type]
    else:
        raise ValueError("Not a valid type")

def getTask (item: dict) -> str:
    description = bs4.BeautifulSoup(item.get('description', ''), 'html.parser').get_text()
    task = bs4.BeautifulSoup(item.get('task', ''), 'html.parser').get_text()
    return (description + " " + task).strip()

def writeItemToSite(item: dict, includeContext : bool = False, context: str = None):
    id = str(item.get('@id', ''))
    type = parseType(item.get('@type'))
    title = bs4.BeautifulSoup(item.get('title', ''), 'html.parser').get_text()
    description = bs4.BeautifulSoup(item.get('description', ''), 'html.parser').get_text()
    task = bs4.BeautifulSoup(item.get('task', ''), 'html.parser').get_text()
    st.write('**Item ID:** ' + id + '  \n**Typ:** ' + type + '  \n**Titel:** ' + title + '  \n**Vignette:** ' + description + '  \n**Aufgabe:** ' + task)
    if len(item.get('answers', [])) > 0:
        st.write('**Antwortoptionen:**')
        for index, answer in enumerate(item.get('answers', [])):
            answerOption = ""
            answerOption += str(index + 1) + ": " + bs4.BeautifulSoup(answer.get('text', ''), 'html.parser').get_text()
            if int(answer.get('points', 0)) > 0:
                answerOption += " (richtig)"
            else:
                answerOption += " (falsch)"
            st.write(answerOption)
    if includeContext is True:
        if context is None:
            context = bs4.BeautifulSoup(item.get('context', ''), 'html.parser').get_text()
        if len(context) > 2:
            st.write("**Kontext:** " + context)
        else:
            st.write("**Kontext:** Keine Informationen vorhanden")

def formatItemForPrompt(item: dict, includeContext: bool = False, context: str = None):
    id = str(item.get('@id', ''))
    type = parseType(item.get('@type', ''))
    title = bs4.BeautifulSoup(item.get('title', ''), 'html.parser').get_text()
    description = bs4.BeautifulSoup(item.get('description', ''), 'html.parser').get_text()
    task = bs4.BeautifulSoup(item.get('task', ''), 'html.parser').get_text()
    text = 'Typ: ' + type + '\nTitel: ' + title + '\nVignette: ' + description + '\nAufgabe: ' + task
    if len(item.get('answers', [])) > 0:
        text += '\nAntwortoptionen:\n'
        for index, answer in enumerate(item.get('answers', [])):
            text += str(index + 1) + ": " + bs4.BeautifulSoup(answer.get('text', ''), 'html.parser').get_text()
            if int(answer.get('points', 0)) > 0:
                text += " (richtig)"
            else:
                text += " (falsch)"
            text += '\n'
    if includeContext is True:
        if context is None:
            context = bs4.BeautifulSoup(item.get('context', ''), 'html.parser').get_text()
        if len(context) > 2:
            text += "\n**Kontext:** " + context
        else:
            text += "\nKontext:"
    return text

def removeIdenticalDimensions(inputJSON: list[dict]) -> list[dict]:
    toReturn = []
    lastIndex = len(inputJSON) - 1
    if lastIndex == 0:
        toReturn = inputJSON
    else:
        tmp = set()
        for i in inputJSON:
            tmp.add(json.dumps(i))
        tmp = list(tmp)
        for i in tmp:
            toReturn.append(json.loads(i))
    return toReturn

@st.cache_resource
def getInstalledModels() -> list[str]:
    models = list(openAIModels.keys())
    localModels = glob.glob(os.path.expanduser('~') + "/.local/share/nomic.ai/GPT4All/*.gguf")
    return models + localModels

@st.cache_resource
def getDomainMaterialFolders() -> list[str]:
    found_folders = [folder for folder in os.listdir("./") if os.path.isdir(folder) and "domain_context" in folder]
    return ['None'] + found_folders

@st.cache_data
def rephraselearnerKnowledge(_llm: BaseLanguageModel, taxonomy_description, context) -> str:
    """Retrieve learner knowledge (what the learner is supposed to know). Currently reading from input file"""
    conversation, _ = createConversation(_llm, True, False)
    task = """## Instructions ##\nFor the given context written in German determine what a leaner (named "tested person") is supposed to know. If no string is given, answer with "unknown"."""
    examples = """
## Examples ##
Input: Mengenlehre wurde im Unterricht behandelt. Der Begriff Disjunktheit wurde definiert und an einem Beispiel wurde gezeigt was es heißt, wenn zwei Mengen Disjunkt sind. Die Vorgehensweise zur Feststellung der Disjunkheit wurde behandelt.
Output: The tested person should know about set theory, the concept of disjointness, and the procedure to test two sets for disjointness.
Input: 
Output: unknown
"""
    prompt = "Input:" + context + "\nOutput:"
    return conversation.predict(input=taxonomy_description + task + examples + prompt)

context_template = """A person to be tested is faced with the task below. We are going to find out what the tested person is supposed to know regardig the task, based on relevant pieces of texts from a higher education course.

## Pieces of text from the course ##
{context}

## Task ##
{question}

## Instructions ##
The only verified pieces of text are between `## Pieces of text from the course ##` and `## Task ##`. Answer solely based on these pieces of text and provide a summary of what the tested person should know regarding the task. Let's think step by step. If you don't know the answer, answer with "unknown". Don't try to make up an answer.

## Answer format ##
Answer with one to five sentences. Answer as precise as possible and use the terms and phrases from the pieces of text from the course.
Start your answer with: The tested person should know ...

Answer:"""

@st.cache_resource
def getEmbeddings():
    return HuggingFaceEmbeddings()

@st.cache_resource
def getContextRetriever(contextFolderPath: str):
    context_retriever = None
    try:
        context_retriever = FAISS.load_local(contextFolderPath + "/context.index", getEmbeddings())
    except:
        loader = DirectoryLoader(contextFolderPath + "/", glob="**/*.pdf", use_multithreading=True, max_concurrency=12, loader_cls=PyPDFLoader, recursive=True)
        context_docs = loader.load_and_split()
        context_retriever = FAISS.from_documents(context_docs, getEmbeddings())
        context_retriever.save_local(contextFolderPath + "/context.index")
    return context_retriever

@st.cache_data
def retrieveStudentContext(_llm: BaseLanguageModel, task, contextFolderPath: str):
    # TODO detect whether fitting documents were found and don't ask the LLM if not documents were found
    # TODO 
    context_chain = RetrievalQA.from_chain_type(
        llm=_llm,
        chain_type="stuff",
        retriever=getContextRetriever(contextFolderPath).as_retriever(),
        chain_type_kwargs={"prompt": PromptTemplate(template=context_template, input_variables=["context", "question"])}
    )
    return context_chain({"query": task})

def rewriteDimensions(input: list[dict]) -> list[dict]:
    processDimensionReduction = FuzzyDict({
        **dict.fromkeys(["recognizing", "identifying", "recalling", "retrieving", "remember"], "remember"),
        **dict.fromkeys(["interpreting", "clarifying", "paraphrasing", "representing", "translating", "exemplifying", "illustrating", "instantiating", "classifying", "categorizing", "subsuming", "summarizing", "abstracting", "generalizing", "inferring", "concluding", "extrapolating", "interpolating", "predicting", "comparing", "contrasting", "mapping", "matching", "explaining", "constructing","models", "understand"], "understand"),
        **dict.fromkeys(["executing", "carrying out", "implementing", "using", "apply"], "apply"),
        **dict.fromkeys(["differentiating", "ciscriminating", "distinguishing", "focusing", "selecting", "organizing", "finding", "coherence", "intergrating", "outlining", "parsing", "structuring", "attributing", "deconstructing", "analyse"], "analyse"),
        **dict.fromkeys(["checking", "coordinating", "detecting", "monitoring", "testing", "critiquing", "judging", "evaluate"], "evaluate"),
        **dict.fromkeys(["generating", "hypothesizing", "planning", "designing", "producing", "constructing", "create"], "create")
    })
    knowledgeDimensionReduction = FuzzyDict({
        **dict.fromkeys(["terminology", "specific details", "specific elements", "factual"], "factual"),
        **dict.fromkeys(["classifications", "categories", "principles", "generalizations", "theories", "models", "structures", "conceptual"], "conceptual"),
        **dict.fromkeys(["skills", "algorithms", "techniques", "methods", "criteria", "procedural"], "procedural"),
        **dict.fromkeys(["strategic", "cognitive tasks", "contextual", "conditional", "self-knowledge", "metacognitive"], "metacognitive"),
        **dict.fromkeys(["", "unknown"], "unknown")
    })
    processDimensions = {"remember", "understand", "apply", "analyse", "evaluate", "create"}
    # We'll keep unknown as there are processes on things that aren't knowledge
    knowledgeDimensions = {"factual", "conceptual", "procedural", "metacognitive", "unknown"}
    result = []
    for index, dim in enumerate(input):
        a = dim.get('processDimension', '').lower().strip()
        b = dim.get('knowledgeDimension', '').lower().strip()
        if(b == ""):
            dim["knowledgeDimension"] = "unknown"
        with suppress(KeyError):
            a = processDimensionReduction[a]
            b = knowledgeDimensionReduction[b]
        if a in processDimensions and b in knowledgeDimensions:
            result.append({"processDimension": a, "knowledgeDimension": b})
    return result

def validateJsonSchema(input) -> bool:
    akSchema = {
        "type": "array",
        "items": {
            "type": "object",
            "properties": {
                "processDimension": {"type": "string"},
                "knowledgeDimension": {"type": "string"}
            },
            "required":["processDimension", "knowledgeDimension"],
            "additionalProperties": False
        }
    }
    try:
        validate(instance=input, schema=akSchema)
    except jsonschema.exceptions.ValidationError as err:
        return False
    return True

def writeResultToItem(item: dict, ak_values: list[dict], thoughts: list[str]):
    json_parser = JSONOutputParser()
    if item.get('annotations') is None:
        item['annotations'] = {}
    try:
        processed_json = json.loads("" + json_parser.parse(ak_values))
        processed_json = rewriteDimensions(processed_json)
        processed_json = removeIdenticalDimensions(processed_json)
        if not validateJsonSchema(processed_json):
            raise ValueError(f"JSON schema won't match: `{processed_json}`")
        item['annotations']['akLLM'] = processed_json
        st.json(processed_json)
        with st.expander("Thoughts:"):
            item['annotations']['akLLMThoughts'] = thoughts
            st.write('\n\n'.join(thoughts))
        st.divider()
    except ValueError as error:
        st.write("Classification not possible: " + error)
