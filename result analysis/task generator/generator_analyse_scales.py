import json
import numpy as np
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, classification_report
import streamlit as st
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import functools

st.set_page_config(layout="wide")

def buildData(item):
    return {
        "id": item["@id"],
        "ca": item["annotations"]["concept_appropriateness"],
        "tcm": item["annotations"]["task_completeness"],
        "tcr": item["annotations"]["task_correctness"],
        # "da": item["annotations"]["didactial_appropriateness"],
        "scm": item["annotations"]["solution_completeness"],
        "scr": item["annotations"]["solution_correctness"],
        }

def compressData(data):
    def mapEntry(entry):
        return {
        "id": entry["id"],
        "ca": np.mean(entry["ca"]),
        "ca_std": np.std(entry["ca"]),
        "tcm": np.mean(entry["tcm"]),
        "tcm_std": np.std(entry["tcm"]),
        "tcr": np.mean(entry["tcr"]),
        "tcr_std": np.std(entry["tcr"]),
        # "da": np.mean(entry["da"]),
        # "da_std": np.std(entry["da"]),
        "scm": np.mean(entry["scm"]),
        "scm_std": np.std(entry["scm"]),
        "scr": np.mean(entry["scr"]),
        "scr_std": np.std(entry["scr"])
        }
    return map(mapEntry, data)

def printTable(header: str, data) :
    '''print a sortable data frame'''
    st.write("""
    Concept Apropriatness: ca  
    Task Completeness: tcm  
    Task Correctness: tcr  
    Didactial Appropriateness: da  
    Solution Completeness: scm  
    Solution Correctness: scr  
    """)
    st.write("Values: Mean, std: Standard Deviation")
    st.header(header)
    st.data_editor(pd.DataFrame(compressData(data)), hide_index=True, )

# ########################### CREATE STREAMLIT-APP ###########################

st.title('Item generator classification result analysis')
st.warning("This is a research prototype and might produce misleading results. Check everything thoroughly!")

file = st.file_uploader('Rated Items', type=['json','jsonld'], help='A file that contains one or more rated items')

items: dict = None
if file is not None:
    items = json.load(file)

if items is not None and len(items) > 0:
    data = []
    for item in items["@graph"]:
        data.append(buildData(item))
    printTable("Full data set", data)

    analyse_key = st.selectbox("Data to analyse", ["ca", "tcm", "tcr", "scm", "scr"])
    if analyse_key is not None:
        sns.set_theme(style="ticks")
        fig, ax = plt.subplots(figsize=(5, 40))

        print(sns.load_dataset("titanic"))
        def mapEntry(entry):
            data = []
            for i in entry[analyse_key]:
                data.append({
                    "id": entry["id"],
                    "value": i
                })
            return data
        df = map(mapEntry, data)
        df = functools.reduce(lambda a, b: a+b, df)
        print(pd.DataFrame(df))
        sns.boxplot(x="value", y="id", data=pd.DataFrame(df), palette="vlag")

        ax.xaxis.grid(True)
        ax.set(ylabel="")
        sns.despine(trim=True, left=True)
        st.pyplot(fig=fig, use_container_width=False)