import csv
import sys
import json
delimiter = '#'
quotechar='`'

def numbersToNames(text: str) -> list[str]:
  processNames = ["remember", "understand", "apply", "analyse", "evaluate", "create"]
  knowledgeNames = ["factual", "conceptual", "procedural", "metacognitive", "unknown"]

  text = text.replace(' ', '').split("/")
  if text[0].isnumeric() and 1 <= int(text[0]) <= 6:
    text[0] = processNames[int(text[0])-1]
  else:
    text[0] = "unknown"

  if text[1].isnumeric() and 1 <= int(text[1]) <= 4:
    text[1] = knowledgeNames[int(text[1])-1]
  else:
    text[1] = "unknown"

  return text

items = {"@graph" : []}

def rewriteType(type):
  if type == "MC":
    return "http://tech4comp/eal/MultipleChoiceItem"
  if type == "SC":
    return "http://tech4comp/eal/SingleChoiceItem"
  if type == "FT":
    return "http://tech4comp/eal/FreeTextItem"
  if "/" in type:
    return type

def addAnswer(type, row, startIndex, item):
  # print(row[startIndex])
  if type == "http://tech4comp/eal/MultipleChoiceItem":
    item["answers"].append({
      "@type": "http://tech4comp/eal/Answer",
      "text": row[startIndex],
      "points": row[startIndex+1],
      "pointsNot": row[startIndex+2]
    })
  if type == "http://tech4comp/eal/SingleChoiceItem":
    item["answers"].append({
      "@type": "http://tech4comp/eal/Answer",
      "text": row[startIndex],
      "points": row[startIndex+1],
    })

def addAnswerOld(type, row, startIndex, item):
  # print(row[startIndex])
  if type == "http://tech4comp/eal/MultipleChoiceItem":
    item["answers"].append({
      "@type": "http://tech4comp/eal/Answer",
      "text": row[startIndex+2],
      "points": row[startIndex+1],
      "pointsNot": row[startIndex]
    })
  if type == "http://tech4comp/eal/SingleChoiceItem":
    item["answers"].append({
      "@type": "http://tech4comp/eal/Answer",
      "text": row[startIndex],
      "points": row[startIndex+1],
    })

def rewriteID(id: str) -> str:
  if "/" in id:
      return id
  else:
      return "http://item.easlit.erzw.uni-leipzig.de/item/" + str(id) + "-old"

def addNewItem(row, items, file, raterName, key):
  # print(row)
  item = {
    "@id": rewriteID(row[1]),
    "@type": rewriteType(row[0]),
    "title": row[3],
    "description": row[4],
    "task": row[5],
    "annotations": {
      key: [],
    },
    "answers": []
  }
  if "-old" in item["@id"]:
    if rewriteType(row[0]) == "http://tech4comp/eal/MultipleChoiceItem":
        for index in map(lambda x: x*3 + 7,range(0,5)):
          addAnswerOld("http://tech4comp/eal/MultipleChoiceItem", row, index, item)
    if rewriteType(row[0]) == "http://tech4comp/eal/SingleChoiceItem":
      for index in map(lambda x: x*2 + 7,range(0,4)):
          addAnswerOld("http://tech4comp/eal/SingleChoiceItem", row, index, item)
  else:
    if rewriteType(row[0]) == "http://tech4comp/eal/MultipleChoiceItem":
        for index in map(lambda x: x*3 + 6,range(0,5)):
          addAnswer("http://tech4comp/eal/MultipleChoiceItem", row, index, item)
    if rewriteType(row[0]) == "http://tech4comp/eal/SingleChoiceItem":
      for index in map(lambda x: x*3 + 6,range(0,4)):
          addAnswer("http://tech4comp/eal/SingleChoiceItem", row, index, item)

  if "/" in row[2]:
    for ak in row[2].split(','):
      item["annotations"][key].append(akFormatter(ak,file, raterName))
  elif " " in row[2] and len(row[2]) < 4:
    row[2] = row[2].replace(" ", "/")
    for ak in row[2].split(','):
      item["annotations"][key].append(akFormatter(ak,file, raterName))
  else:
    print("Skipping item due to unreadable classification")
  items["@graph"].append(item)

def akFormatter(ak, file, raterName):
  return {
          "RaterName": raterName,
          "processDimension": numbersToNames(ak.replace("//", "/"))[0],
          "knowledgeDimension": numbersToNames(ak.replace("//", "/"))[1]
        }

def rewriteRater(file) -> str:
  if "AM" in file:
    return "Anne Martin"
  if "LK" in file:
    return "Laura Köbis"
  if "LS" in file:
    return "Lisa Stechert"
  if "NP" in file:
    return "Norbert Pengel"
  if "MH" in file:
    return "Maike Haag"
  if "WW" in file:
    return "HW Wollersheim"

def addToExistingItem(row, item, file, raterName, key):
  if "llm" in raterName.lower():
    item["annotations"]["akLLM"] = []
    for ak in row[2].split(','):
      item["annotations"]["akLLM"].append(akFormatter(ak,file, raterName))
  else:
    if "/" in row[2]:
      try: item["annotations"][key]
      except KeyError:
        item["annotations"][key] = []
      for ak in row[2].split(','):
        item["annotations"][key].append(akFormatter(ak,file, raterName))
    elif " " in row[2] and len(row[2]) < 4:
      try: item["annotations"][key]
      except KeyError:
        item["annotations"][key] = []
      row[2] = row[2].replace(" ", "/")
      for ak in row[2].split(','):
        item["annotations"][key].append(akFormatter(ak,file, raterName))
    else:
      print("Skipping item due to unreadable classification")

def addToItems(file, items, raterName, key="akManual"):
  with open(file, 'r', encoding="utf8") as csvfile:
      reader = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)
      for row in reader:
        if reader.line_num > 1:
          print("Processing line " + str(reader.line_num))
          added = False
          for item in items["@graph"]:
            if item["@id"] == rewriteID(row[1]):
              addToExistingItem(row, item, file, raterName, key)
              added = True
          if not added:
            addNewItem(row, items, file, raterName, key)

if len(sys.argv) <= 1:
  print("Too few CSV files provided")
  sys.exit()

if sys.argv[1] not in ["-g", "-i"]:
  print('Use like:')
  print('For inter-rater comparisons:')
  print('python ./generator_CSV_to_JSON.py -i file1_rater1.csv [...] file1_rater2.csv [...] output.json')
  print()
  print('For rater-classifier comparisons:')
  print('python ./classifier_CSV_to_JSON.py -g rater1.csv ... classifier.json output.json')

elif sys.argv [1] == "-i":
  print('Creating a file that compares human values against human values')
  print(sys.argv)
  for idx, x in enumerate(sys.argv):# script, option, file1, file2, output
    if idx <= 1:
      continue
    elif idx < len(sys.argv) - 1:
      if idx < ((len(sys.argv) - 3) / 2) + 2:
        print("Processing file " + str(sys.argv[idx]) + " as Rater_A")
        addToItems(sys.argv[idx], items, rewriteRater(sys.argv[idx]), "Rater_A")
      else:
        print("Processing file " + str(sys.argv[idx]) + " as Rater_B")
        addToItems(sys.argv[idx], items, rewriteRater(sys.argv[idx]), "Rater_B")

elif sys.argv [1] == "-g":
  print('Creating a file that compares classified values against human values')
  print(sys.argv)
  if len(sys.argv) >= 4:
    addToItems(sys.argv[2], items, rewriteRater(sys.argv[2]))

  for index in range(2, len(sys.argv) - 1):
    if index + 1 < len(sys.argv) - 1:
      if not sys.argv[index + 1].endswith(".json"):
        addToItems(sys.argv[index + 1], items, rewriteRater(sys.argv[index + 1]))
      else:
        addToItems(sys.argv[index + 1], items, "LLM", "akLLM")

with open(sys.argv[-1], 'w', encoding='utf-8') as f:
            json.dump(items, f, ensure_ascii=False, indent=2)
