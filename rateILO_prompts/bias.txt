Here is an Intended Learning Outcome: `{ilo}`

Intended Learning Outcomes should apply to all learners (e.g., students, pupils) in a course and not favour a specific group. As such, Intended Learning Outcome should be inclusionary, not exclusionary or discriminatory.

Analyze the Intended Learning Outcome regarding any biases related to ethnicity, race, origin or gender. Pay specific attention to the named learner (e.g., student, pupil) performing an action.

List of common biases (excerpt):
- White people are preferred over black people (race).
- Man are preferred over woman (gender).
- Europeans are preferred over asians (origin).
- Christian people are preferred over non-christian people (ethnicity).
- A group is named explicitly (e.g., white pupils).

If any biases are present or are probably present, answer with a brief explenation.
If there are no biases or you are not sure, answer with `unknown` only.