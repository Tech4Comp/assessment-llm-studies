#! /usr/bin/env node
const fs = require('fs');
const ft = require('./db_ft.json');
const mc = require('./db_mc.json');
const sc = require('./db_sc.json');

items = [...ft,...mc,...sc]

let id = 1

for (item of items) {
  item["@id"] = id
  if(item["type"] === "FT") {
    item["@type"] = "http://tech4comp/eal/FreeTextItem"
    delete item.type
    item.task = item.question
    delete item.question
    // console.log(item)
  }

  if(item["type"] === "MC") {
    item["@type"] = "http://tech4comp/eal/MultipleChoiceItem"
    delete item.type
    item.task = item.question
    delete item.question
    item.answers = []
    for (i = 1; i <= 31; i++) {
      if(item["answer" + i] !== "") {
        let toInsert = { "@type": "http://tech4comp/eal/Answer" }
        toInsert.text = "" + item['answer' + i]
        toInsert.points = item['points' + i]
        toInsert.pointsNotSelected = item['pointsnot' + i]
        item.answers.push(toInsert)
      }
      delete item['answer' + i]
      delete item['points' + i]
      delete item['pointsnot' + i]
    }
    // console.log(item)
  }

  if(item["type"] === "SC") {
    item["@type"] = "http://tech4comp/eal/SingleChoiceItem"
    delete item.type
    item.task = item.question
    delete item.question
    item.answers = []
    for (i = 1; i <= 15; i++) {
      if(item["answer" + i] !== "") {
        let toInsert = { "@type": "http://tech4comp/eal/Answer" }
        toInsert.text = "" + item['answer' + i]
        toInsert.points = item['points' + i]
        item.answers.push(toInsert)
      }
      delete item['answer' + i]
      delete item['points' + i]
    }
    // console.log(item)
  }

  item.levelAnnotation = {}
  item.levelAnnotation.chosen = item.level
  delete item.level

  id = id + 1
}

let result = {
  "@context": {
    "modificationTime": "http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#dateModified",
    "creationTime": "http://purl.org/healthcarevocab/v1#CreationTime",
    "title": "http://purl.org/dc/terms/title",
    "description": "http://tech4comp/eal/context",
    "task": "http://tech4comp/eal/task",
    "images": {
      "@id": "http://schema.org/image",
      "@container": "@set"
    },
    "difficulty": {
      "@id": "http://tech4comp/eal/difficulty",
      "@type": "http://www.w3.org/2001/XMLSchema#integer"
    },
    "expectedSolvability": {
      "@id": "http://tech4comp/eal/expectedSolvability",
      "@type": "http://www.w3.org/2001/XMLSchema#double"
    },
    "performanceLevel": {
      "@id": "http://tech4comp/eal/hasPerformanceLevel",
      "@type": "@id"
    },
    "knowledgeDimension": {
      "@id": "http://tech4comp/eal/hasKnowledgeDimension",
      "@type": "@id"
    },
    "learningOutcome": {
      "@id": "http://tech4comp/eal/hasLearningOutcome",
      "@type": "@id"
    },
    "annotations": {
      "@id": "http://tech4comp/eal/annotation"
    },
    "flagged": {
      "@id": "http://tech4comp/eal/flagged",
      "@type": "http://www.w3.org/2001/XMLSchema#boolean"
    },
    "note": "http://www.w3.org/2004/02/skos/core#note",
    "bloom": {
      "@id": "http://tech4comp/eal/hasBloomMapping",
      "@container": "@set"
    },
    "points": {
      "@id": "http://tech4comp/eal/points",
      "@type": "http://www.w3.org/2001/XMLSchema#decimal"
    },
    "status": "http://tech4comp/eal/status",
    "project": {
      "@id": "http://tech4comp/eal/hasProject",
      "@type": "@id"
    },
    "contributors": {
      "@id": "http://purl.org/dc/terms/contributor",
      "@container": "@set",
      "@type": "@id"
    },
    "topics": {
      "@id": "http://purl.org/dc/terms/subject",
      "@container": "@set",
      "@type": "@id"
    },
    "answers": {
      "@id": "http://tech4comp/eal/hasAnswer",
      "@container": "@list"
    },
    "text": {
      "@id": "http://tech4comp/eal/answerText"
    },
    "pointsNotSelected": {
      "@id": "http://tech4comp/eal/pointsNotSelected",
      "@type": "http://www.w3.org/2001/XMLSchema#decimal"
    },
    "maximumSelectableAnswers": {
      "@id": "http://tech4comp/eal/maximumSelectableAnswers",
      "@type": "http://www.w3.org/2001/XMLSchema#integer"
    },
    "minimumSelectableAnswers": {
      "@id": "http://tech4comp/eal/minimumSelectableAnswers",
      "@type": "http://www.w3.org/2001/XMLSchema#integer"
    },
    "remoteItemURL": "http://tech4comp/eal/remoteItemURL",
    "sequenceStep": {
      "@id": "http://tech4comp/eal/sequenceStep",
      "@type": "http://www.w3.org/2001/XMLSchema#integer"
    }
  },
  "@graph": items
}

fs.writeFileSync('db.json', JSON.stringify(result));