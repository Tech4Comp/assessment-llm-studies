#! /usr/bin/env node
const Fuse = require('fuse.js')
const fs = require('fs')
const util = require('util')
var _ = require('lodash')
const stripHtml = require("string-strip-html")
let newData = JSON.parse(fs.readFileSync('./biwi_2023_06.jsonld', 'utf8'))
let oldData = JSON.parse(fs.readFileSync('./biwi.json', 'utf8'))
let newDataCopy = JSON.parse(fs.readFileSync('./biwi_2023_06.jsonld', 'utf8'))

function replaceHTMLinItem (item) {
  item.title = stripHtml.stripHtml(item.title === undefined ? "" : item.title).result.trim().replace(/\n/g,' ')
  item.description = stripHtml.stripHtml(item.description === undefined ? "" : item.description).result.trim().replace(/\n/g,' ')
  item.task = stripHtml.stripHtml(item.task === undefined ? "" : item.task).result.trim().replace(/\n/g,' ')
}

for (item of newData["@graph"]) {
  replaceHTMLinItem(item)
  item.task = (item.description + item.task).replace(/ /g,'').replace(/ /g,'')
}

for (item of newDataCopy["@graph"]) {
  replaceHTMLinItem(item)
}

for (item of oldData["@graph"]) {
  replaceHTMLinItem(item)
  item.task = (item.description + item.task).replace(/ /g,'').replace(/ /g,'')
}

const fuseNewData = new Fuse(newData["@graph"].map(a => { return {id: a["@id"], task: a.task}}), {
  keys: ['task'],
  includeScore: true
})

const fuseOldData = new Fuse(oldData["@graph"].map(a => { return {id: a["@id"], task: a.task}}), {
  keys: ['task'],
  includeScore: true
})

function findIdenticalItems(data, fuseData, threshold = 0.1, verbose = false) {// 0.1 scheint eine gute Grenze zu sein - per try & error bestimmt
  let scores = []
  let sameItems = []
  for (item of data["@graph"].map(a => { return {id: a["@id"], task: a.task}})) {
    const results = fuseData.search(item.task).sort((a,b) => a.score >= b.score ? 1 : -1)
    scores.push(results[0].score)
      if(results[0].score >= threshold && results[0].score <= threshold*2 && verbose){
        console.log("Searching for:\n" + item.task)
        console.log("Found:\n" + results[0].item.task)
        console.log("Score: " + results[0].score)
        console.log("")
      }
      if(results[0].score <= threshold) {
        sameItems.push({new: item.id, old: results[0].item.id})
    }
  }
  if(verbose) {
    scores.sort((a,b) => a > b ? 1 : -1)
    console.log(JSON.stringify(scores)) 
  }
  return sameItems
}

let identicalItems = []
console.log("Searching in old data set...")
identicalItems = [...identicalItems, ...findIdenticalItems(newData, fuseOldData, 0.05, false)] // find new Items in old data
console.log("Searching in new data set...")
identicalItems = [...identicalItems, ...(findIdenticalItems(oldData, fuseNewData, 0.05, false).map(a => {return {new: a.old, old: a.new}}))] // find old items in new data

// deduplicate identical Items
identicalItems = identicalItems.filter((value, index, self) =>
  index === self.findIndex((t) => (
    t.old === value.old && t.new === value.new
  ))
)

console.log("Identical Items:")
console.log(JSON.stringify(identicalItems.sort((a,b) => a.new > b.new ? 1 : -1)))

function findAdditionalItems(identicalItems = [], data, mappingFunc) {
  let additionaItems = []
  let foundItems = new Set(identicalItems.map(mappingFunc))
  for (item of data["@graph"]) {
    if(!foundItems.has(item["@id"]))
    additionaItems.push(item["@id"])
  }
  console.log(JSON.stringify(additionaItems.sort((a,b) => a > b ? 1 : -1)))
  console.log("Amount: " + additionaItems.length)
  return additionaItems
}


console.log("\nAdditional Items in new data set:")
let additionaItems = findAdditionalItems(identicalItems, newData, (a) => a.new)

console.log("\nAdditional Items in old data set:")
findAdditionalItems(identicalItems, oldData, (a) => a.old)

additionaItems = additionaItems.map( id => {
  return newDataCopy["@graph"].find(item => item["@id"] === id)
})

//write to json file
fs.writeFileSync("./additionalItems.json", JSON.stringify(additionaItems))

//write to csv file
const { stringify } = require("csv-stringify");
const writableStream = fs.createWriteStream("additionalItems.csv");

const columns = [
  "type",
  "id",
  "title",
  "description",
  "task"
];
for (a in [0,1,2,3,4]) {
  columns.push("answer" + a)
  columns.push("points" + a)
  columns.push("pointsNot" + a)
}

const stringifier = stringify({ header: true, columns: columns });
for (item of additionaItems) {
  let toWrite = [item["@type"], item["@id"], item.title, item.description, item.task]
  if(!_.isEmpty(item.answers)){
    for (answer of item.answers) {
      toWrite.push(answer.text)
      toWrite.push(answer.points)
      toWrite.push(answer.pointsNotSelected)
    }
  }
  stringifier.write(toWrite);
}
stringifier.pipe(writableStream);