import os
import streamlit as st
import json
import sys
from langchain.prompts import PromptTemplate
from langchain.callbacks import get_openai_callback
from pydantic import BaseModel, Field
from typing import List
import re
import glob
from utils import createConversation, writeItemToSite, formatItemForPrompt, load_model, getInstalledModels, retrieveStudentContext, getTask, rephraselearnerKnowledge, writeResultToItem, getDomainMaterialFolders

st.set_page_config(layout="wide")

# ########################### PREPARE PROMPTs ###########################
# TODO Ideas for improvement:
# - let llm outline the solution path to find processes and knowledge
# - Anforderungsniveau der Aufgabe einschätzen - Menge und Dichte an Denkoperationen + benötigtes Vorwissen bzw. Verknüpfung mit anderen Inhalten = Anforderungsniveau

ak_context = open("./itemToTaxonomy_prompts/ak_context.txt").read()

# from https://learnlab.org/wiki/images/9/9f/Krathwohl_Bloom%27s_taxonomy_revised.pdf
taxonomy_description = open("./itemToTaxonomy_prompts/taxonomy_description.txt").read()

general_instructions = """
## Instructions ##
You will be asked to analyse a given assessment item and classify it according to the taxonomy matrix.

For the considered assessment item:
1. Analyse the item regarding to the required cognitive processes of the item type.
2. Determine the task to be performed by the tested person.
3. With regard to the given context, determine what the tested person should already know.
4. For the identified task and given the knowledge that the tested person is supposed to possess, identifiy cognitive processes related to specific knowledge, that have to be used to solve the assessment item.
5. Output the identified cognitive processes and related knowledge in the specified format.

We will do this step by step.
"""

example = open("./itemToTaxonomy_prompts/FsCoT_example.txt").read()

task1 = """
## Assessment Item ##
{item}

## Step 1 ##
Analyse the above assessment item written in German regarding to the required cognitive processes of the item type.
Complete step 1 only.
"""
task2 = """
## Step 2 ##
For the above assessment item, determine the task to be performed.
Complete step 2 only.
"""
task3 = """
## Step 3 ##
For the given context (Kontext) of the above assessment item, determine what the tested person should already know. If no context is given, answer with "unknown".
Complete step 3 only.
"""
task4 = """
## Step 4 ##
For the identified task of the above assessment item and given the knowledge that the tested person is supposed to possess, identifiy cognitive processes related to specific knowledge, that have to be used to solve the assessment item. 
Complete step 4 only.
"""

output_formatter = """
## Step 5 ##
""" + open("./itemToTaxonomy_prompts/output_formatter.txt").read() + """

Complete step 5 only.
"""

classify_ak_prompt = PromptTemplate(
    template=task1,
    input_variables=["item"]
)

# ########################### Additional Functions ###########################

@st.cache_data
def processItem(_llm, item, context = None) -> list:
    """Processes the given Item with CoT and returns the individual thoughts"""
    conversation, memory = createConversation(_llm, True)
    text = formatItemForPrompt(item, True, context)
    thoughts = []
    print("test2")
    thoughts.append(conversation.predict(input=ak_context + taxonomy_description + general_instructions + example + classify_ak_prompt.format(item=text)))
    print("test3")
    print(thoughts[0])
    thoughts.append(conversation.predict(input=task2))
    print(thoughts[1])
    # thoughts.append(conversation.predict(input=task3))
    studentKnowledge = "unknown"
    if context is None:
        studentKnowledge = rephraselearnerKnowledge(_llm, taxonomy_description, item.get('context', ''))
    else:
        studentKnowledge = context
    if "unknown" in studentKnowledge:
            studentKnowledge = "It is not known what the person being tested knows."
    memory.save_context({"input": task3}, {"output": studentKnowledge})
    thoughts.append(studentKnowledge)
    print(thoughts[2])
    thoughts.append(conversation.predict(input=task4))
    print(thoughts[3])
    thoughts.append(conversation.predict(input=output_formatter))
    print(thoughts[4])
    return thoughts

# ########################### CREATE STREAMLIT-APP ###########################

st.title('Item taxonomy classification using LLMs')
st.warning("This is a research prototype and might produce misleading results. Check everything thoroughly!")

########################### SEARCH & INIT LLM ###########################

models = getInstalledModels()
chosenModel = st.selectbox('Model', (models), format_func=lambda path: re.split('/', path)[-1])
if chosenModel != '':
    llm = load_model(chosenModel)

########################### ACTUAL APP ###########################

folders = getDomainMaterialFolders()
contextFolder = st.selectbox('Domain Material Folder', (folders))

uploaded_file = st.file_uploader('Items', type=['json','jsonld'], help='A file that contains one or more items')

if 'costs' not in st.session_state:
    st.session_state['costs'] = 0

items: list = None
if uploaded_file is not None:
    items = json.load(uploaded_file)

processNumberOfItems = st.number_input("Stop after x Items:", min_value=1, value=1, step=1)

st.write("Taxonomy: Anderson & Krathwohl (24 levels)")

start = st.button("Start")

if start:
    if items is not None and len(items) > 0:
        with st.spinner('Processing...'):
            with get_openai_callback() as cb:
                for index, item in enumerate(items['@graph']):
                    try:
                        if contextFolder != "None":
                            context = retrieveStudentContext(llm, getTask(item), contextFolder)
                            print(context)
                            writeItemToSite(item, True, context.get('result', 'unknown'))
                            thoughts = processItem(llm, item, context.get('result', 'unknown'))
                        else:
                            writeItemToSite(item, True)
                            thoughts = processItem(llm, item)
                        output = thoughts.pop()
                        writeResultToItem(item, output, thoughts)
                        if index + 1 == processNumberOfItems:
                            break
                    except Exception as e:
                        print(e)
            print(cb)
            st.session_state.costs = st.session_state.costs + cb.total_cost
        st.success('Done!')
        with open('result.json', 'w', encoding='utf-8') as f:
            json.dump(items, f, ensure_ascii=False, indent=2)
    else:
        st.write("Can't start, something's missing")

if st.session_state.costs:
    st.write("Total Cost (USD Cent): ¢{:.4f}".format((st.session_state.costs * 100)))