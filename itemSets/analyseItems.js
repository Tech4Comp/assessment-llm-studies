#! /usr/bin/env node
const fs = require('fs');
let data = JSON.parse(fs.readFileSync('./biwi_2023_06.jsonld', 'utf8'));

console.log("Items without topics:")

let toReport = []
for (item of data["@graph"]) {
  topics = item.annotations.topics || []

  if(topics.length < 1)
    toReport.push(item["@id"])
}
console.log(toReport, toReport.length)

console.log("Items without taxonomy:")

toReport = []
for (item of data["@graph"]) {
  taxo = item.annotations.bloom || []

  if(taxo.length < 1)
    toReport.push(item["@id"])
}
console.log(toReport, toReport.length)

console.log("Items without LOs:")

toReport = []
for (item of data["@graph"]) {
  lo = item.learningOutcome

  if(lo === undefined)
    toReport.push(item["@id"])
}
console.log(toReport, toReport.length)