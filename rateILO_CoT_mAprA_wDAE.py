import os
import streamlit as st
import json
import sys
from langchain.prompts import PromptTemplate
from langchain.callbacks import get_openai_callback
from pydantic import BaseModel, Field
from typing import List
import re
from utils import createConversation, load_model, getInstalledModels

st.set_page_config(layout="wide")

# ########################### PREPARE CHAINs ###########################

definitions = open("./rateILO_prompts/definitions.txt").read()
ilo_aspects = open("./rateILO_prompts/ILO_aspects.txt").read()
ilo_examples = open("./rateILO_prompts/ILO_examples.txt").read()

# Nichts nehmen, was nicht enthalten ist. GGf. per Agent gegenchecken
evaluate = definitions + ilo_aspects + ilo_examples + """
The following string is to be evaluated and potentially improved. It is most probably an Intended Learning Outcome, but might also be a Learning Objective: {ilo}

1. Thoroughly assess the given Intended Learning Outcome based on the provided definitions, guidelines, and examples only.
2. Assign a quality rating to the provided Intended Learning Outcome on a scale from 1 (worst) to 10 (best).
3. Present a concise list of potential improvements for the provided Intended Learning Outcome, ensuring that all listed items can be applied to it.

Do not rephrase the Intended Learning Outcome yet. Do not list improvements the given Intended Learning Outcome already satisfies.

Answer according to this template:
Quality: 5/10

Potential improvements:
- ...
"""

evaluatePrompt = PromptTemplate(
    template=evaluate, 
    input_variables=["ilo"]
)

evaluateAnswer = definitions + ilo_aspects + """
Given Intended Learning Outcome: {ilo}

List of potential improvements:
{list}

Assess the given Intended Learning Outcome to determine which of the potential improvements have already been implemented and which ones are not applicable. Provide a list of the remaining improvements without altering the quality rating. Answer using the following template:
Quality:

Potential improvements:
- ...
"""

evaluateAnswerPrompt = PromptTemplate(
    template=evaluateAnswer, 
    input_variables=["ilo", "list"]
)

bias = open("./rateILO_prompts/bias.txt").read()

biasPrompt = PromptTemplate(
    template=bias, 
    input_variables=["ilo"]
)

languagePrompt = PromptTemplate(
    template="What's the language of the following string? Answer with the language only.\n\n{input}", 
    input_variables=["input"]
)

translationPrompt = PromptTemplate(
    template="Translate the given string into English. Answer with the translated string only.\n\n{input}", 
    input_variables=["input"]
)

improve_ilo = ilo_aspects + """
Given Intended Learning Outcome: {ilo}

{list}

Provide an improved Intended Learning Outcome in {lang} that addresses all the listed potential improvements for the given Intended Learning Outcome. Answer with the improved Intended Learning Outcome only.
"""

improveILOPrompt = PromptTemplate(
    template=improve_ilo, 
    input_variables=["lang", "ilo", "list"]
)

############ Streamlit

st.title('Intended Learning Outcome evaluation using LLMs')

models = getInstalledModels()
chosenModel = st.selectbox('Model', (models), format_func=lambda path: re.split('/', path)[-1])
if chosenModel != '':
    llm = load_model(chosenModel)

EvaluateILOConversation, _ = createConversation(llm)
EvaluateEvaluationConversation, _ = createConversation(llm)
BiasConversation, _ = createConversation(llm)
LanguageConversation, _ = createConversation(llm)
ImprovementConversation, _ = createConversation(llm)

if 'costs' not in st.session_state:
    st.session_state['costs'] = 0

ilo = st.text_area("Intended Learning Outcome")

@st.cache_data
def getLanguage(ilo: str) -> str:
    return LanguageConversation.predict(input=languagePrompt.format(input=ilo))

@st.cache_data
def translateIlo(ilo: str) -> str:
    return LanguageConversation.predict(input=translationPrompt.format(input=ilo))

@st.cache_data
def evaluate(ilo: str) -> str:
    st.session_state.costs = 0
    improvements = EvaluateILOConversation.predict(input=evaluatePrompt.format(ilo=ilo))
    print(improvements)
    checkedImprovements = EvaluateEvaluationConversation.predict(input=evaluateAnswerPrompt.format(ilo=ilo, list=improvements))
    print(checkedImprovements)
    return checkedImprovements

@st.cache_data
def detectBiases(ilo: str) -> str:
    return BiasConversation.predict(input=biasPrompt.format(ilo=ilo))

@st.cache_data
def improve(ilo: str, improvements: str, language: str) -> str:
    return ImprovementConversation.predict(input=improveILOPrompt.format(lang=language, ilo=ilo, list=improvements))

if ilo != "":
    with st.spinner('Processing...'):
        with get_openai_callback() as cb:
            language = getLanguage(ilo)
            translatedIlo = ilo
            if "english" not in language.lower():
                translatedIlo = translateIlo(ilo)
            improvements = evaluate(translatedIlo)
            st.warning("This is a research prototype and might produce misleading results. Check everything thoroughly!")
            st.write(improvements)
            potentialBiases = detectBiases(translatedIlo)
            print(potentialBiases)
            if len(potentialBiases) > 10:
                st.write(potentialBiases)
            if st.button("Improve Intended Learning Outcome"):
                improvedILO = improve(translatedIlo, improvements, language)
                st.write(improvedILO)
            print(cb)
            st.session_state.costs = st.session_state.costs + cb.total_cost

if st.session_state.costs:
    st.write("Total Cost (USD Cent): ¢{:.4f}".format((st.session_state.costs * 100)))