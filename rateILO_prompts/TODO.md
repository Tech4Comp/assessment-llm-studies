## Definitions not used yet

Course Goals or Learning Goals are descriptions of broad desired results of a course. Goals reflect the purpose of the course and may be derived from a program of study. They are what students are expexted to learn or get out of a course on a abstract level. Courses are split up into several teaching-learning situations.

... Learning Objectives identify discrete aspects of a Goal.
Learning Outcomes, in contrast to Intended Learning Outcomes, refer to the measured results of what learners know, understand and are able to do.

## Aspects not used yet
- Be realistically attainable within the given time frame of the teaching-learning situation.
- Be categorized according to the degrees of complexity, such as Anderson & Krathwohl's taxonomy for the cognitive domain.
- Aim for the highest taxonomy level feasible for the specific teaching-learning situation.
- Align with the prescriptive competencies outlined in a module, course, or curriculum.
- Comprise a transparent and evident processes employed to achieve the outcome.