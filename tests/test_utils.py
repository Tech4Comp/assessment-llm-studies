import unittest
from utils import rewriteDimensions, removeIdenticalDimensions

class TestRewriteDimensions(unittest.TestCase): 
    def test_rewrite_dimensions(self): 
        input = [{"processDimension": "analyse", "knowledgeDimension": "classify"}]
        result = [{"processDimension": "analyse", "knowledgeDimension": "conceptual"}]
        self.assertEqual(rewriteDimensions(input), result)

        input = [{"processDimension": "analyse", "knowledgeDimension": ""}]
        result = [{"processDimension": "analyse", "knowledgeDimension": "unknown"}]
        self.assertEqual(rewriteDimensions(input), result)

        input = [{"processDimension": "analyse", "knowledgeDimension": "test"}]
        result = []
        self.assertEqual(rewriteDimensions(input), result)

        input = [{"processDimension": "analy", "knowledgeDimension": "classify"}]
        result = [{"processDimension": "analyse", "knowledgeDimension": "conceptual"}]
        self.assertEqual(rewriteDimensions(input), result)

        input = [{
            "processDimension": "remember",
            "knowledgeDimension": "conceptual"
        },
        {
            "processDimension": "analyze",
            "knowledgeDimension": "unknown"
        },
        {
            "processDimension": "evaluate",
            "knowledgeDimension": "unknown"
        },
        {
            "processDimension": "execute",
            "knowledgeDimension": "procedural"
        }]
        result = [{
            "processDimension": "remember",
            "knowledgeDimension": "conceptual"
        },
        {
            "processDimension": "analyse",
            "knowledgeDimension": "unknown"
        },
        {
            "processDimension": "evaluate",
            "knowledgeDimension": "unknown"
        },
        {
            "processDimension": "apply",
            "knowledgeDimension": "procedural"
        }]
        self.assertEqual(rewriteDimensions(input), result)

class TestRemoveIdenticalDimensions(unittest.TestCase): 
    def test_remove_identical_dimensions(self): 
        input = [{"processDimension": "analyse", "knowledgeDimension": "conceptual"}]
        result = [{"processDimension": "analyse", "knowledgeDimension": "conceptual"}]
        self.assertEqual(removeIdenticalDimensions(input), result)

        input = [
            {"processDimension": "analyse", "knowledgeDimension": "conceptual"},
            {"processDimension": "analyse", "knowledgeDimension": "conceptual"}
        ]
        result = [{"processDimension": "analyse", "knowledgeDimension": "conceptual"}]
        self.assertEqual(removeIdenticalDimensions(input), result)

if __name__ == '__main__':
    unittest.main()