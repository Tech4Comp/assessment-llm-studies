import csv
import sys
import json
import bs4
delimiter = '`'
quotechar='"'

def numbersToNames(text: str) -> list[str]:
  processNames = ["remember", "understand", "apply", "analyse", "evaluate", "create"]
  knowledgeNames = ["factual", "conceptual", "precedural", "metacognitive", "unknown"]
  
  text = text.replace(' ', '').split("/")
  if text[0].isnumeric() and 1 <= int(text[0]) <= 6:
    text[0] = processNames[int(text[0])-1]
  else:
    text[0] = "unknown"

  if text[1].isnumeric() and 1 <= int(text[1]) <= 4:
    text[1] = knowledgeNames[int(text[1])-1]
  else:
    text[1] = "unknown"

  return text

items = {"@graph" : []}

def cleanString(text: str) -> str:
  if text == "":
    return text
  else:
    return bs4.BeautifulSoup(text, 'html.parser').get_text().replace("\n", " ").replace("  ", " ").strip()

def initialItems(file, items):
  with open(file, 'r') as csvfile:
      reader = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)
      for row in reader:
        if reader.line_num > 1:
          item = {
            "@id": row[1],
            "@type": row[0],
            "title": cleanString(row[3]),
            "description": cleanString(row[4]),
            "task": cleanString(row[5]),
            "annotations": {
              "akManual": [
                {
                  "comment": "Rated by Andreas Thor",
                  "processDimension": numbersToNames(row[2])[0],
                  "knowledgeDimension": numbersToNames(row[2])[1]
                }
              ],
            }
          }

          items["@graph"].append(item)

if len(sys.argv) <= 1:
  print("Too few CSV files provided")
  sys.exit()

if len(sys.argv) >= 1:
  print("Processing " + sys.argv[1])
  initialItems(sys.argv[1], items)

for index in range(1, len(sys.argv)):
  if index + 1 < len(sys.argv):
    print("Processing " + sys.argv[index + 1])
    initialItems(sys.argv[index + 1], items)

with open(sys.argv[1] + ".json", 'w', encoding='utf-8') as f:
            json.dump(items, f, ensure_ascii=False, indent=2)
